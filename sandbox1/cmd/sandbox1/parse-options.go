// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package main

import (
	"fmt"

	"github.com/jessevdk/go-flags"
)

func parseOptions() (options, error) {
	var result options
	args, err := flags.Parse(&result)
	if err != nil {
		if et, ok := err.(*flags.Error); ok {
			if et.Type == flags.ErrHelp {
				return result, nil
			}
		}
		return result, fmt.Errorf("error parsing flags: %v", err)
	}
	if len(args) > 0 {
		return result, fmt.Errorf("unexpected arguments: %v", args)
	}

	return result, err
}
