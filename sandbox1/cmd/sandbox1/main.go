// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package main

import (
	"fmt"
	"log"

	usecases "gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox/sandbox1/internal/usecase"
	"go.uber.org/zap"
)

type options struct {
	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	logger, err := newZapLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	financieleVerplichtingTypeenUsecase := usecases.NewFinancieleVerplichtingTypeenUsecase(
		logger,
	)

	err = financieleVerplichtingTypeenUsecase.GenerateSchema()
	if err != nil {
		logger.Fatal("--- FinancieleVerplichtingType GenerateSchema: FAILED ---", zap.Error(err))
	} else {
		logger.Info("--- FinancieleVerplichtingType GenerateSchema: SUCCEEDED ---")
	}

	financieleVerplichtingTypeSlice, err := financieleVerplichtingTypeenUsecase.ValidateAndUnmarshalYaml()
	if err != nil {
		logger.Fatal("--- FinancieleVerplichtingType ValidateAndUnmarshalYaml: FAILED ---", zap.Error(err))
	} else {
		logger.Info("--- FinancieleVerplichtingType ValidateAndUnmarshalYaml: SUCCEEDED ---")
	}

	err = financieleVerplichtingTypeenUsecase.GenerateMarkdown(financieleVerplichtingTypeSlice)
	if err != nil {
		logger.Fatal("--- FinancieleVerplichtingType GenerateMarkdown: FAILED ---", zap.Error(err))
	} else {
		logger.Info("--- FinancieleVerplichtingType GenerateMarkdown: SUCCEEDED ---")
	}

	err = financieleVerplichtingTypeenUsecase.GenerateGoCode(financieleVerplichtingTypeSlice)
	if err != nil {
		logger.Fatal("--- FinancieleVerplichtingType GenerateGoEnum: FAILED ---", zap.Error(err))
	} else {
		logger.Info("--- FinancieleVerplichtingType GenerateGoEnum: SUCCEEDED ---")
	}

	err = financieleVerplichtingTypeenUsecase.GenerateDartCode(financieleVerplichtingTypeSlice)
	if err != nil {
		logger.Fatal("--- FinancieleVerplichtingType GenerateDartCode: FAILED ---", zap.Error(err))
	} else {
		logger.Info("--- FinancieleVerplichtingType GenerateDartCode: SUCCEEDED ---")
	}
}

func newZapLogger(logOptions LogOptions) (*zap.Logger, error) {
	config := logOptions.ZapConfig()
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return logger, nil
}
