cd "$(dirname "$0")"

rm -rf dist
mkdir dist


rm -rf pkg/model
mkdir pkg/model

go run cmd/sandbox1/*.go

gofmt -w pkg/model

prettier dist --write