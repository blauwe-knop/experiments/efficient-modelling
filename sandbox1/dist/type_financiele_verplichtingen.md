# Type Financiele Verplichtingen

In het onderstaande tabel is er een overzicht van de type financiele verplichtingen binnen het Vorderingenoverzicht Rijk.

| Type                                                    | Parameter                                          | Description                                        |
| ------------------------------------------------------- | -------------------------------------------------- | -------------------------------------------------- |
| DUO_WET_STUDIEFINANCIERING                              | Studieschuld                                       | Studieschuld                                       |
| DUO_WET_INBURGERING_LENING                              | Lening inburgering                                 | Lening inburgering                                 |
| DUO_WET_INBURGERING_MEDISCHE_ONTHEFFING                 | Kosten medische ontheffing                         | Kosten medische ontheffing                         |
| DUO_OV_BOETE                                            | OV-Boete                                           | OV-Boete                                           |
| CJIB_WAHV                                               | Verkeersboete                                      | Verkeersboete                                      |
| CJIB_ADMINISTRATIEKOSTEN                                | Administratiekosten                                | Administratiekosten                                |
| CJIB_KOSTEN_EERSTE_AANMANING                            | Kosten eerste aanmaning                            | Kosten eerste aanmaning                            |
| CJIB_CAK_BESTUURSRECHTELIJKE_PREMIE_ZVW                 | Bestuursrechtelijke premie ZVW                     | Bestuursrechtelijke premie ZVW                     |
| BD_INKOMENSHEFFING                                      | Heffing inkomstenbelasting                         | Heffing inkomstenbelasting                         |
| BD_BELASTINGRENTE                                       | Belastingrente                                     | Belastingrente                                     |
| BD_MOTORRIJTUIGENBELASTING                              | Motorrijtuigenbelasting                            | Motorrijtuigenbelasting                            |
| BD_ZORGVERZEKERINGSWET                                  | Premie zorgverzekeringswet                         | Premie zorgverzekeringswet                         |
| BD_ZORGVERZEKERINGSWET_BIJDRAGE                         | Inkomensafhankelijke bijdrage zorgverzekeringswet  | Inkomensafhankelijke bijdrage zorgverzekeringswet  |
| BD_ZORGTOESLAG                                          | Zorgtoeslag                                        | Zorgtoeslag                                        |
| GEMEENTE_OZB_EIGENAAR                                   | Gemeentebelastingen                                | OZB eigenaar                                       |
| GEMEENTE_RIOOLHEFFING                                   | Gemeentebelastingen                                | Rioolheffing eigenaar                              |
| GEMEENTE_AFVALSTOFFENHEFFING                            | Gemeentebelastingen                                | Afvalstoffenheffing                                |
| RVO_DGF                                                 | Dierengezondheidsfonds                             | Aanslag Dierengezondheidsfonds                     |
| RVO_BOETE                                               | Boete                                              | Opgelegde boete                                    |
| RVO_FOUT_BETAALORGAAN                                   | Fout betaalorgaan                                  | Fout betaalorgaan                                  |
| RVO_HEFFING                                             | Heffing                                            | Heffing                                            |
| RVO_MEERJAREN_SANCTIE                                   | Meerjaren sanctie                                  | Meerjaren sanctie                                  |
| RVO_ONREGELMATIGHEID                                    | Onregelmatigheid                                   | Onregelmatigheid                                   |
| RVO_RANDVOORWAARDE                                      | Randvoorwaarde                                     | Randvoorwaarde                                     |
| RVO_SANCTIE                                             | Sanctie                                            | Opgelegde sanctie                                  |
| RVO_TUSSENTIJDSE_VORDERING                              | Tussentijdse vordering                             | Opgelegde tussentijdse vordering                   |
| RVO_VASTSTELLING                                        | Vaststelling                                       | Vaststelling                                       |
| RVO_VORDERING                                           | Vordering                                          | Opgelegde vordering                                |
| RVO_WETTELIJKE_RENTE                                    | Wettelijke rente                                   | Berekende wettelijke rente                         |
| CAK_WLZ                                                 | Eigen bijdrage WLZ                                 | Eigen bijdrage WLZ                                 |
| CAK_WMO                                                 | Eigen bijdrage WMO                                 | Eigen bijdrage WMO                                 |
| CAK_ZVW_WAN                                             | Zorgverzekeringswet wanbetalersregeling            | Zorgverzekeringswet wanbetalersregeling            |
| CJIB_BUITENGERECHTELIJKE_AFDOENING_VAN_STRAFBARE_FEITEN | Buitengerechtelijke afdoening van strafbare feiten | Buitengerechtelijke afdoening van strafbare feiten |
| CJIB_OVERHEIDSVORDERING                                 | Overheidsvordering                                 | Overheidsvordering                                 |
| CJIB_OVERHEIDSVORDERING_INNEN_EN_INCASSO                | Overheidsvorderingen Innen en Incasso              | Overheidsvorderingen Innen en Incasso              |
| CJIB_TRANSACTIEVOORSTEL                                 | Transactievoorstel                                 | Transactievoorstel                                 |
| CJIB_EUROPESE_GELDELIJKE_SANCTIE                        | Europese geldelijke sanctie                        | Europese geldelijke sanctie                        |
| CJIB_WAHV_BESCHIKKING                                   | WAHV Beschikking                                   | WAHV Beschikking                                   |
| CJIB_WAHV_BESCHIKKINGEN_MULDER_BOETES                   | WAHV beschikkingen (Mulder boetes)                 | WAHV beschikkingen (Mulder boetes)                 |
| CJIB_WAARBORGSOM_SCHORSING_VOORLOPIGE_HECHTENIS         | Waarborgsom schorsing voorlopige hechtenis         | Waarborgsom schorsing voorlopige hechtenis         |
| CJIB_VOORWAARDELIJK_SEPOT_SCHADEVERGOEDING              | Voorwaardelijk sepot schadevergoeding              | Voorwaardelijk sepot schadevergoeding              |
| CJIB_SCHADEVERGOEDING_ALS_BIJZONDERE_VOORWAARDE         | Schadevergoeding als bijzondere voorwaarde         | Schadevergoeding als bijzondere voorwaarde         |
| CJIB_BOETEVONNISSEN_STRABIS                             | Boetevonnissen (STRABIS)                           | Boetevonnissen (STRABIS)                           |
| CJIB_WAARBORGSOM                                        | Waarborgsom                                        | Waarborgsom                                        |
| CJIB_STORTING_SCHADEFONDS                               | Storting Schadefonds                               | Storting Schadefonds                               |
| CJIB_GELDSOM_VOORWAARDELIJKE_GRATIE                     | Geldsom voorwaardelijke gratie                     | Geldsom voorwaardelijke gratie                     |
| CJIB_CONFISCATIEBESLISSINGEN                            | Confiscatiebeslissingen                            | Confiscatiebeslissingen                            |
| CJIB_SCHIKKINGEN                                        | Schikkingen                                        | Schikkingen                                        |
| CJIB_OM_AFDOENING                                       | OM-afdoening                                       | OM-afdoening                                       |
| CJIB_ONTNEMINGSMAATREGELEN_PLUKZE                       | Ontnemingsmaatregelen (PLUKZE)                     | Ontnemingsmaatregelen (PLUKZE)                     |
| CJIB_SCHADEVERGOEDINGSMAATREGELEN_VONNISSEN             | Schadevergoedingsmaatregelen vonnissen             | Schadevergoedingsmaatregelen vonnissen             |
| RMB_HEFFING                                             | Heffing belasting                                  | Heffing nelasting                                  |
| RMB_FONDS_HEFFING                                       | Heffing fonds                                      | Aanslag heffing fonds                              |
| RCI_BOETE                                               | Boete overtreding                                  | Boete overtreding                                  |
| RCI_ADMINISTRATIEKOSTEN                                 | Administratiekosten                                | Administratiekosten                                |
| RCI_KOSTEN_EERSTE_AANMANING                             | Kosten eerste aanmaning                            | Kosten eerste aanmaning                            |
| NIO_WET_FINANCIERING                                    | Schuld wet financiering                            | Schuld wet financiering                            |
| NIO_WET_ABC_LENING                                      | Lening wet ABC                                     | Lening wet ABC                                     |
| NIO_WET_ABC_ONTHEFFING                                  | Kosten ABC ontheffing                              | Kosten ABC ontheffing                              |
