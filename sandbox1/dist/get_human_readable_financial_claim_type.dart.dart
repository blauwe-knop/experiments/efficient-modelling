// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

String getHumanReadableFinancialClaimName(String financialClaimType) {
  switch (financialClaimType) {
    case "DUO_WET_STUDIEFINANCIERING":
      return "Studieschuld";
    case "DUO_WET_INBURGERING_LENING":
      return "Lening inburgering";
    case "DUO_WET_INBURGERING_MEDISCHE_ONTHEFFING":
      return "Kosten medische ontheffing";
    case "DUO_OV_BOETE":
      return "OV-Boete";
    case "CJIB_WAHV":
      return "Verkeersboete";
    case "CJIB_ADMINISTRATIEKOSTEN":
      return "Administratiekosten";
    case "CJIB_KOSTEN_EERSTE_AANMANING":
      return "Kosten eerste aanmaning";
    case "CJIB_CAK_BESTUURSRECHTELIJKE_PREMIE_ZVW":
      return "Bestuursrechtelijke premie ZVW";
    case "BD_INKOMENSHEFFING":
      return "Heffing inkomstenbelasting";
    case "BD_BELASTINGRENTE":
      return "Belastingrente";
    case "BD_MOTORRIJTUIGENBELASTING":
      return "Motorrijtuigenbelasting";
    case "BD_ZORGVERZEKERINGSWET":
      return "Premie zorgverzekeringswet";
    case "BD_ZORGVERZEKERINGSWET_BIJDRAGE":
      return "Inkomensafhankelijke bijdrage zorgverzekeringswet";
    case "BD_ZORGTOESLAG":
      return "Zorgtoeslag";
    case "GEMEENTE_OZB_EIGENAAR":
      return "Gemeentebelastingen";
    case "GEMEENTE_RIOOLHEFFING":
      return "Gemeentebelastingen";
    case "GEMEENTE_AFVALSTOFFENHEFFING":
      return "Gemeentebelastingen";
    case "RVO_DGF":
      return "Dierengezondheidsfonds";
    case "RVO_BOETE":
      return "Boete";
    case "RVO_FOUT_BETAALORGAAN":
      return "Fout betaalorgaan";
    case "RVO_HEFFING":
      return "Heffing";
    case "RVO_MEERJAREN_SANCTIE":
      return "Meerjaren sanctie";
    case "RVO_ONREGELMATIGHEID":
      return "Onregelmatigheid";
    case "RVO_RANDVOORWAARDE":
      return "Randvoorwaarde";
    case "RVO_SANCTIE":
      return "Sanctie";
    case "RVO_TUSSENTIJDSE_VORDERING":
      return "Tussentijdse vordering";
    case "RVO_VASTSTELLING":
      return "Vaststelling";
    case "RVO_VORDERING":
      return "Vordering";
    case "RVO_WETTELIJKE_RENTE":
      return "Wettelijke rente";
    case "CAK_WLZ":
      return "Eigen bijdrage WLZ";
    case "CAK_WMO":
      return "Eigen bijdrage WMO";
    case "CAK_ZVW_WAN":
      return "Zorgverzekeringswet wanbetalersregeling";
    case "CJIB_BUITENGERECHTELIJKE_AFDOENING_VAN_STRAFBARE_FEITEN":
      return "Buitengerechtelijke afdoening van strafbare feiten";
    case "CJIB_OVERHEIDSVORDERING":
      return "Overheidsvordering";
    case "CJIB_OVERHEIDSVORDERING_INNEN_EN_INCASSO":
      return "Overheidsvorderingen Innen en Incasso";
    case "CJIB_TRANSACTIEVOORSTEL":
      return "Transactievoorstel";
    case "CJIB_EUROPESE_GELDELIJKE_SANCTIE":
      return "Europese geldelijke sanctie";
    case "CJIB_WAHV_BESCHIKKING":
      return "WAHV Beschikking";
    case "CJIB_WAHV_BESCHIKKINGEN_MULDER_BOETES":
      return "WAHV beschikkingen (Mulder boetes)";
    case "CJIB_WAARBORGSOM_SCHORSING_VOORLOPIGE_HECHTENIS":
      return "Waarborgsom schorsing voorlopige hechtenis";
    case "CJIB_VOORWAARDELIJK_SEPOT_SCHADEVERGOEDING":
      return "Voorwaardelijk sepot schadevergoeding";
    case "CJIB_SCHADEVERGOEDING_ALS_BIJZONDERE_VOORWAARDE":
      return "Schadevergoeding als bijzondere voorwaarde";
    case "CJIB_BOETEVONNISSEN_STRABIS":
      return "Boetevonnissen (STRABIS)";
    case "CJIB_WAARBORGSOM":
      return "Waarborgsom";
    case "CJIB_STORTING_SCHADEFONDS":
      return "Storting Schadefonds";
    case "CJIB_GELDSOM_VOORWAARDELIJKE_GRATIE":
      return "Geldsom voorwaardelijke gratie";
    case "CJIB_CONFISCATIEBESLISSINGEN":
      return "Confiscatiebeslissingen";
    case "CJIB_SCHIKKINGEN":
      return "Schikkingen";
    case "CJIB_OM_AFDOENING":
      return "OM-afdoening";
    case "CJIB_ONTNEMINGSMAATREGELEN_PLUKZE":
      return "Ontnemingsmaatregelen (PLUKZE)";
    case "CJIB_SCHADEVERGOEDINGSMAATREGELEN_VONNISSEN":
      return "Schadevergoedingsmaatregelen vonnissen";
    case "RMB_HEFFING":
      return "Heffing belasting";
    case "RMB_FONDS_HEFFING":
      return "Heffing fonds";
    case "RCI_BOETE":
      return "Boete overtreding";
    case "RCI_ADMINISTRATIEKOSTEN":
      return "Administratiekosten";
    case "RCI_KOSTEN_EERSTE_AANMANING":
      return "Kosten eerste aanmaning";
    case "NIO_WET_FINANCIERING":
      return "Schuld wet financiering";
    case "NIO_WET_ABC_LENING":
      return "Lening wet ABC";
    case "NIO_WET_ABC_ONTHEFFING":
      return "Kosten ABC ontheffing";
    default:
      return financialClaimType;
  }
}

String getHumanReadableFinancialClaimEventName(String financialClaimType) {
  switch (financialClaimType) {
    case "DUO_WET_STUDIEFINANCIERING":
      return "Studieschuld";
    case "DUO_WET_INBURGERING_LENING":
      return "Lening inburgering";
    case "DUO_WET_INBURGERING_MEDISCHE_ONTHEFFING":
      return "Kosten medische ontheffing";
    case "DUO_OV_BOETE":
      return "OV-Boete";
    case "CJIB_WAHV":
      return "Verkeersboete";
    case "CJIB_ADMINISTRATIEKOSTEN":
      return "Administratiekosten";
    case "CJIB_KOSTEN_EERSTE_AANMANING":
      return "Kosten eerste aanmaning";
    case "CJIB_CAK_BESTUURSRECHTELIJKE_PREMIE_ZVW":
      return "Bestuursrechtelijke premie ZVW";
    case "BD_INKOMENSHEFFING":
      return "Heffing inkomstenbelasting";
    case "BD_BELASTINGRENTE":
      return "Belastingrente";
    case "BD_MOTORRIJTUIGENBELASTING":
      return "Motorrijtuigenbelasting";
    case "BD_ZORGVERZEKERINGSWET":
      return "Premie zorgverzekeringswet";
    case "BD_ZORGVERZEKERINGSWET_BIJDRAGE":
      return "Inkomensafhankelijke bijdrage zorgverzekeringswet";
    case "BD_ZORGTOESLAG":
      return "Zorgtoeslag";
    case "GEMEENTE_OZB_EIGENAAR":
      return "OZB eigenaar";
    case "GEMEENTE_RIOOLHEFFING":
      return "Rioolheffing eigenaar";
    case "GEMEENTE_AFVALSTOFFENHEFFING":
      return "Afvalstoffenheffing";
    case "RVO_DGF":
      return "Aanslag Dierengezondheidsfonds";
    case "RVO_BOETE":
      return "Opgelegde boete";
    case "RVO_FOUT_BETAALORGAAN":
      return "Fout betaalorgaan";
    case "RVO_HEFFING":
      return "Heffing";
    case "RVO_MEERJAREN_SANCTIE":
      return "Meerjaren sanctie";
    case "RVO_ONREGELMATIGHEID":
      return "Onregelmatigheid";
    case "RVO_RANDVOORWAARDE":
      return "Randvoorwaarde";
    case "RVO_SANCTIE":
      return "Opgelegde sanctie";
    case "RVO_TUSSENTIJDSE_VORDERING":
      return "Opgelegde tussentijdse vordering";
    case "RVO_VASTSTELLING":
      return "Vaststelling";
    case "RVO_VORDERING":
      return "Opgelegde vordering";
    case "RVO_WETTELIJKE_RENTE":
      return "Berekende wettelijke rente";
    case "CAK_WLZ":
      return "Eigen bijdrage WLZ";
    case "CAK_WMO":
      return "Eigen bijdrage WMO";
    case "CAK_ZVW_WAN":
      return "Zorgverzekeringswet wanbetalersregeling";
    case "CJIB_BUITENGERECHTELIJKE_AFDOENING_VAN_STRAFBARE_FEITEN":
      return "Buitengerechtelijke afdoening van strafbare feiten";
    case "CJIB_OVERHEIDSVORDERING":
      return "Overheidsvordering";
    case "CJIB_OVERHEIDSVORDERING_INNEN_EN_INCASSO":
      return "Overheidsvorderingen Innen en Incasso";
    case "CJIB_TRANSACTIEVOORSTEL":
      return "Transactievoorstel";
    case "CJIB_EUROPESE_GELDELIJKE_SANCTIE":
      return "Europese geldelijke sanctie";
    case "CJIB_WAHV_BESCHIKKING":
      return "WAHV Beschikking";
    case "CJIB_WAHV_BESCHIKKINGEN_MULDER_BOETES":
      return "WAHV beschikkingen (Mulder boetes)";
    case "CJIB_WAARBORGSOM_SCHORSING_VOORLOPIGE_HECHTENIS":
      return "Waarborgsom schorsing voorlopige hechtenis";
    case "CJIB_VOORWAARDELIJK_SEPOT_SCHADEVERGOEDING":
      return "Voorwaardelijk sepot schadevergoeding";
    case "CJIB_SCHADEVERGOEDING_ALS_BIJZONDERE_VOORWAARDE":
      return "Schadevergoeding als bijzondere voorwaarde";
    case "CJIB_BOETEVONNISSEN_STRABIS":
      return "Boetevonnissen (STRABIS)";
    case "CJIB_WAARBORGSOM":
      return "Waarborgsom";
    case "CJIB_STORTING_SCHADEFONDS":
      return "Storting Schadefonds";
    case "CJIB_GELDSOM_VOORWAARDELIJKE_GRATIE":
      return "Geldsom voorwaardelijke gratie";
    case "CJIB_CONFISCATIEBESLISSINGEN":
      return "Confiscatiebeslissingen";
    case "CJIB_SCHIKKINGEN":
      return "Schikkingen";
    case "CJIB_OM_AFDOENING":
      return "OM-afdoening";
    case "CJIB_ONTNEMINGSMAATREGELEN_PLUKZE":
      return "Ontnemingsmaatregelen (PLUKZE)";
    case "CJIB_SCHADEVERGOEDINGSMAATREGELEN_VONNISSEN":
      return "Schadevergoedingsmaatregelen vonnissen";
    case "RMB_HEFFING":
      return "Heffing nelasting";
    case "RMB_FONDS_HEFFING":
      return "Aanslag heffing fonds";
    case "RCI_BOETE":
      return "Boete overtreding";
    case "RCI_ADMINISTRATIEKOSTEN":
      return "Administratiekosten";
    case "RCI_KOSTEN_EERSTE_AANMANING":
      return "Kosten eerste aanmaning";
    case "NIO_WET_FINANCIERING":
      return "Schuld wet financiering";
    case "NIO_WET_ABC_LENING":
      return "Lening wet ABC";
    case "NIO_WET_ABC_ONTHEFFING":
      return "Kosten ABC ontheffing";
    default:
      return financialClaimType;
  }
}
