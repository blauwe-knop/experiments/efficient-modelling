// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

type FinancieleVerplichtingType struct {
	// typecode uit de VO Rijk vorderingtypenlijst
	Type string
	// De financiele zaak naam behorende bij deze type vordering
	FinancieleZaaknaam string
	// De gebeurtenis naam behorende bij deze type vordering
	GebeurtenisNaam string
}
