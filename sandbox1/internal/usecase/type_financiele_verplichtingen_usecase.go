// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package usecases

import (
	"encoding/json"
	"html/template"
	"os"
	"path"
	"path/filepath"

	"github.com/ghodss/yaml"
	"github.com/invopop/jsonschema"
	"github.com/stoewer/go-strcase"
	"github.com/xeipuuv/gojsonschema"
	"gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox/sandbox1/internal/model"
	"go.uber.org/zap"
)

type FinancieleVerplichtingTypeenUsecase struct {
	Logger *zap.Logger
}

func NewFinancieleVerplichtingTypeenUsecase(
	logger *zap.Logger,
) *FinancieleVerplichtingTypeenUsecase {
	return &FinancieleVerplichtingTypeenUsecase{
		Logger: logger,
	}
}

func (uc *FinancieleVerplichtingTypeenUsecase) GenerateSchema() error {
	reflector := new(jsonschema.Reflector)

	reflector.KeyNamer = strcase.LowerCamelCase
	reflector.BaseSchemaID = "https://vorijk.nl/schema"

	if err := reflector.AddGoComments("gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox/sandbox1", "./"); err != nil {
		return err
	}

	schema := reflector.Reflect(&[]model.FinancieleVerplichtingType{})

	bytes, err := schema.MarshalJSON()
	if err != nil {
		return err
	}

	err = os.WriteFile("dist/type_financiele_verplichting.schema.json", bytes, 0644)
	if err != nil {
		return err

	}

	return nil
}

func (uc *FinancieleVerplichtingTypeenUsecase) ValidateAndUnmarshalYaml() ([]model.FinancieleVerplichtingType, error) {
	schemaPath := "dist/type_financiele_verplichting.schema.json"

	schemaBytes, err := os.ReadFile(schemaPath)
	if err != nil {
		return nil, err
	}

	yamlDocumentPath := "type_financiele_verplichtingen.yaml"

	yamlDocumentBytes, err := os.ReadFile(yamlDocumentPath)
	if err != nil {
		return nil, err
	}

	jsonDocumentBytes, err := yaml.YAMLToJSON(yamlDocumentBytes)
	if err != nil {
		return nil, err
	}

	schemaLoader := gojsonschema.NewStringLoader(string(schemaBytes))

	documentLoader := gojsonschema.NewStringLoader(string(jsonDocumentBytes))

	validator, err := gojsonschema.Validate(schemaLoader, documentLoader)
	if err != nil {
		return nil, err
	}

	if !validator.Valid() {
		return nil, err
	}

	financieleVerplichtingTypeSlice := []model.FinancieleVerplichtingType{}
	err = json.Unmarshal(jsonDocumentBytes, &financieleVerplichtingTypeSlice)
	if err != nil {
		return nil, err
	}

	return financieleVerplichtingTypeSlice, nil
}

func (uc *FinancieleVerplichtingTypeenUsecase) GenerateMarkdown(financieleVerplichtingTypeSlice []model.FinancieleVerplichtingType) error {
	templatePath := filepath.Join("template/markdown", "type_financiele_verplichtingen.template.md")

	fileTemplatePath := path.Join(templatePath)

	templateFile, err := template.ParseFiles(fileTemplatePath)
	if err != nil {
		return err
	}

	outputFile, err := os.Create("dist/type_financiele_verplichtingen.md")
	if err != nil {
		return err
	}
	defer outputFile.Close()

	err = templateFile.Execute(
		outputFile,
		financieleVerplichtingTypeSlice,
	)
	if err != nil {
		return err
	}

	return nil
}

func (uc *FinancieleVerplichtingTypeenUsecase) GenerateGoCode(financieleVerplichtingTypeSlice []model.FinancieleVerplichtingType) error {
	templatePath := filepath.Join("template/go", "type_financiele_verplichting.go.template")

	fileTemplatePath := path.Join(templatePath)

	templateFile, err := template.ParseFiles(fileTemplatePath)
	if err != nil {
		return err
	}

	outputFile, err := os.Create("pkg/model/type_financiele_verplichting.go")
	if err != nil {
		return err
	}
	defer outputFile.Close()

	err = templateFile.Execute(
		outputFile,
		financieleVerplichtingTypeSlice,
	)
	if err != nil {
		return err
	}

	return nil
}

func (uc *FinancieleVerplichtingTypeenUsecase) GenerateDartCode(financieleVerplichtingTypeSlice []model.FinancieleVerplichtingType) error {
	templatePath := filepath.Join("template/dart", "get_human_readable_financial_claim_type.dart.template")

	fileTemplatePath := path.Join(templatePath)

	templateFile, err := template.ParseFiles(fileTemplatePath)
	if err != nil {
		return err
	}

	outputFile, err := os.Create("dist/get_human_readable_financial_claim_type.dart.dart")
	if err != nil {
		return err
	}
	defer outputFile.Close()

	err = templateFile.Execute(
		outputFile,
		financieleVerplichtingTypeSlice,
	)
	if err != nil {
		return err
	}

	return nil
}
