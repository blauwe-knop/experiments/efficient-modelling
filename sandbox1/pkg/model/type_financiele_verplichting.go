// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

import (
	"fmt"

	systemModel "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"
)

const (
	DUO_WET_STUDIEFINANCIERING                              systemModel.FinancieleVerplichtingType = "DUO_WET_STUDIEFINANCIERING"
	DUO_WET_INBURGERING_LENING                              systemModel.FinancieleVerplichtingType = "DUO_WET_INBURGERING_LENING"
	DUO_WET_INBURGERING_MEDISCHE_ONTHEFFING                 systemModel.FinancieleVerplichtingType = "DUO_WET_INBURGERING_MEDISCHE_ONTHEFFING"
	DUO_OV_BOETE                                            systemModel.FinancieleVerplichtingType = "DUO_OV_BOETE"
	CJIB_WAHV                                               systemModel.FinancieleVerplichtingType = "CJIB_WAHV"
	CJIB_ADMINISTRATIEKOSTEN                                systemModel.FinancieleVerplichtingType = "CJIB_ADMINISTRATIEKOSTEN"
	CJIB_KOSTEN_EERSTE_AANMANING                            systemModel.FinancieleVerplichtingType = "CJIB_KOSTEN_EERSTE_AANMANING"
	CJIB_CAK_BESTUURSRECHTELIJKE_PREMIE_ZVW                 systemModel.FinancieleVerplichtingType = "CJIB_CAK_BESTUURSRECHTELIJKE_PREMIE_ZVW"
	BD_INKOMENSHEFFING                                      systemModel.FinancieleVerplichtingType = "BD_INKOMENSHEFFING"
	BD_BELASTINGRENTE                                       systemModel.FinancieleVerplichtingType = "BD_BELASTINGRENTE"
	BD_MOTORRIJTUIGENBELASTING                              systemModel.FinancieleVerplichtingType = "BD_MOTORRIJTUIGENBELASTING"
	BD_ZORGVERZEKERINGSWET                                  systemModel.FinancieleVerplichtingType = "BD_ZORGVERZEKERINGSWET"
	BD_ZORGVERZEKERINGSWET_BIJDRAGE                         systemModel.FinancieleVerplichtingType = "BD_ZORGVERZEKERINGSWET_BIJDRAGE"
	BD_ZORGTOESLAG                                          systemModel.FinancieleVerplichtingType = "BD_ZORGTOESLAG"
	GEMEENTE_OZB_EIGENAAR                                   systemModel.FinancieleVerplichtingType = "GEMEENTE_OZB_EIGENAAR"
	GEMEENTE_RIOOLHEFFING                                   systemModel.FinancieleVerplichtingType = "GEMEENTE_RIOOLHEFFING"
	GEMEENTE_AFVALSTOFFENHEFFING                            systemModel.FinancieleVerplichtingType = "GEMEENTE_AFVALSTOFFENHEFFING"
	RVO_DGF                                                 systemModel.FinancieleVerplichtingType = "RVO_DGF"
	RVO_BOETE                                               systemModel.FinancieleVerplichtingType = "RVO_BOETE"
	RVO_FOUT_BETAALORGAAN                                   systemModel.FinancieleVerplichtingType = "RVO_FOUT_BETAALORGAAN"
	RVO_HEFFING                                             systemModel.FinancieleVerplichtingType = "RVO_HEFFING"
	RVO_MEERJAREN_SANCTIE                                   systemModel.FinancieleVerplichtingType = "RVO_MEERJAREN_SANCTIE"
	RVO_ONREGELMATIGHEID                                    systemModel.FinancieleVerplichtingType = "RVO_ONREGELMATIGHEID"
	RVO_RANDVOORWAARDE                                      systemModel.FinancieleVerplichtingType = "RVO_RANDVOORWAARDE"
	RVO_SANCTIE                                             systemModel.FinancieleVerplichtingType = "RVO_SANCTIE"
	RVO_TUSSENTIJDSE_VORDERING                              systemModel.FinancieleVerplichtingType = "RVO_TUSSENTIJDSE_VORDERING"
	RVO_VASTSTELLING                                        systemModel.FinancieleVerplichtingType = "RVO_VASTSTELLING"
	RVO_VORDERING                                           systemModel.FinancieleVerplichtingType = "RVO_VORDERING"
	RVO_WETTELIJKE_RENTE                                    systemModel.FinancieleVerplichtingType = "RVO_WETTELIJKE_RENTE"
	CAK_WLZ                                                 systemModel.FinancieleVerplichtingType = "CAK_WLZ"
	CAK_WMO                                                 systemModel.FinancieleVerplichtingType = "CAK_WMO"
	CAK_ZVW_WAN                                             systemModel.FinancieleVerplichtingType = "CAK_ZVW_WAN"
	CJIB_BUITENGERECHTELIJKE_AFDOENING_VAN_STRAFBARE_FEITEN systemModel.FinancieleVerplichtingType = "CJIB_BUITENGERECHTELIJKE_AFDOENING_VAN_STRAFBARE_FEITEN"
	CJIB_OVERHEIDSVORDERING                                 systemModel.FinancieleVerplichtingType = "CJIB_OVERHEIDSVORDERING"
	CJIB_OVERHEIDSVORDERING_INNEN_EN_INCASSO                systemModel.FinancieleVerplichtingType = "CJIB_OVERHEIDSVORDERING_INNEN_EN_INCASSO"
	CJIB_TRANSACTIEVOORSTEL                                 systemModel.FinancieleVerplichtingType = "CJIB_TRANSACTIEVOORSTEL"
	CJIB_EUROPESE_GELDELIJKE_SANCTIE                        systemModel.FinancieleVerplichtingType = "CJIB_EUROPESE_GELDELIJKE_SANCTIE"
	CJIB_WAHV_BESCHIKKING                                   systemModel.FinancieleVerplichtingType = "CJIB_WAHV_BESCHIKKING"
	CJIB_WAHV_BESCHIKKINGEN_MULDER_BOETES                   systemModel.FinancieleVerplichtingType = "CJIB_WAHV_BESCHIKKINGEN_MULDER_BOETES"
	CJIB_WAARBORGSOM_SCHORSING_VOORLOPIGE_HECHTENIS         systemModel.FinancieleVerplichtingType = "CJIB_WAARBORGSOM_SCHORSING_VOORLOPIGE_HECHTENIS"
	CJIB_VOORWAARDELIJK_SEPOT_SCHADEVERGOEDING              systemModel.FinancieleVerplichtingType = "CJIB_VOORWAARDELIJK_SEPOT_SCHADEVERGOEDING"
	CJIB_SCHADEVERGOEDING_ALS_BIJZONDERE_VOORWAARDE         systemModel.FinancieleVerplichtingType = "CJIB_SCHADEVERGOEDING_ALS_BIJZONDERE_VOORWAARDE"
	CJIB_BOETEVONNISSEN_STRABIS                             systemModel.FinancieleVerplichtingType = "CJIB_BOETEVONNISSEN_STRABIS"
	CJIB_WAARBORGSOM                                        systemModel.FinancieleVerplichtingType = "CJIB_WAARBORGSOM"
	CJIB_STORTING_SCHADEFONDS                               systemModel.FinancieleVerplichtingType = "CJIB_STORTING_SCHADEFONDS"
	CJIB_GELDSOM_VOORWAARDELIJKE_GRATIE                     systemModel.FinancieleVerplichtingType = "CJIB_GELDSOM_VOORWAARDELIJKE_GRATIE"
	CJIB_CONFISCATIEBESLISSINGEN                            systemModel.FinancieleVerplichtingType = "CJIB_CONFISCATIEBESLISSINGEN"
	CJIB_SCHIKKINGEN                                        systemModel.FinancieleVerplichtingType = "CJIB_SCHIKKINGEN"
	CJIB_OM_AFDOENING                                       systemModel.FinancieleVerplichtingType = "CJIB_OM_AFDOENING"
	CJIB_ONTNEMINGSMAATREGELEN_PLUKZE                       systemModel.FinancieleVerplichtingType = "CJIB_ONTNEMINGSMAATREGELEN_PLUKZE"
	CJIB_SCHADEVERGOEDINGSMAATREGELEN_VONNISSEN             systemModel.FinancieleVerplichtingType = "CJIB_SCHADEVERGOEDINGSMAATREGELEN_VONNISSEN"
	RMB_HEFFING                                             systemModel.FinancieleVerplichtingType = "RMB_HEFFING"
	RMB_FONDS_HEFFING                                       systemModel.FinancieleVerplichtingType = "RMB_FONDS_HEFFING"
	RCI_BOETE                                               systemModel.FinancieleVerplichtingType = "RCI_BOETE"
	RCI_ADMINISTRATIEKOSTEN                                 systemModel.FinancieleVerplichtingType = "RCI_ADMINISTRATIEKOSTEN"
	RCI_KOSTEN_EERSTE_AANMANING                             systemModel.FinancieleVerplichtingType = "RCI_KOSTEN_EERSTE_AANMANING"
	NIO_WET_FINANCIERING                                    systemModel.FinancieleVerplichtingType = "NIO_WET_FINANCIERING"
	NIO_WET_ABC_LENING                                      systemModel.FinancieleVerplichtingType = "NIO_WET_ABC_LENING"
	NIO_WET_ABC_ONTHEFFING                                  systemModel.FinancieleVerplichtingType = "NIO_WET_ABC_ONTHEFFING"
)

type EventType struct {
	Type systemModel.FinancieleVerplichtingType `json:"type" jsonschema:"enum=DUO_WET_STUDIEFINANCIERING,enum=DUO_WET_INBURGERING_LENING,enum=DUO_WET_INBURGERING_MEDISCHE_ONTHEFFING,enum=DUO_OV_BOETE,enum=CJIB_WAHV,enum=CJIB_ADMINISTRATIEKOSTEN,enum=CJIB_KOSTEN_EERSTE_AANMANING,enum=CJIB_CAK_BESTUURSRECHTELIJKE_PREMIE_ZVW,enum=BD_INKOMENSHEFFING,enum=BD_BELASTINGRENTE,enum=BD_MOTORRIJTUIGENBELASTING,enum=BD_ZORGVERZEKERINGSWET,enum=BD_ZORGVERZEKERINGSWET_BIJDRAGE,enum=BD_ZORGTOESLAG,enum=GEMEENTE_OZB_EIGENAAR,enum=GEMEENTE_RIOOLHEFFING,enum=GEMEENTE_AFVALSTOFFENHEFFING,enum=RVO_DGF,enum=RVO_BOETE,enum=RVO_FOUT_BETAALORGAAN,enum=RVO_HEFFING,enum=RVO_MEERJAREN_SANCTIE,enum=RVO_ONREGELMATIGHEID,enum=RVO_RANDVOORWAARDE,enum=RVO_SANCTIE,enum=RVO_TUSSENTIJDSE_VORDERING,enum=RVO_VASTSTELLING,enum=RVO_VORDERING,enum=RVO_WETTELIJKE_RENTE,enum=CAK_WLZ,enum=CAK_WMO,enum=CAK_ZVW_WAN,enum=CJIB_BUITENGERECHTELIJKE_AFDOENING_VAN_STRAFBARE_FEITEN,enum=CJIB_OVERHEIDSVORDERING,enum=CJIB_OVERHEIDSVORDERING_INNEN_EN_INCASSO,enum=CJIB_TRANSACTIEVOORSTEL,enum=CJIB_EUROPESE_GELDELIJKE_SANCTIE,enum=CJIB_WAHV_BESCHIKKING,enum=CJIB_WAHV_BESCHIKKINGEN_MULDER_BOETES,enum=CJIB_WAARBORGSOM_SCHORSING_VOORLOPIGE_HECHTENIS,enum=CJIB_VOORWAARDELIJK_SEPOT_SCHADEVERGOEDING,enum=CJIB_SCHADEVERGOEDING_ALS_BIJZONDERE_VOORWAARDE,enum=CJIB_BOETEVONNISSEN_STRABIS,enum=CJIB_WAARBORGSOM,enum=CJIB_STORTING_SCHADEFONDS,enum=CJIB_GELDSOM_VOORWAARDELIJKE_GRATIE,enum=CJIB_CONFISCATIEBESLISSINGEN,enum=CJIB_SCHIKKINGEN,enum=CJIB_OM_AFDOENING,enum=CJIB_ONTNEMINGSMAATREGELEN_PLUKZE,enum=CJIB_SCHADEVERGOEDINGSMAATREGELEN_VONNISSEN,enum=RMB_HEFFING,enum=RMB_FONDS_HEFFING,enum=RCI_BOETE,enum=RCI_ADMINISTRATIEKOSTEN,enum=RCI_KOSTEN_EERSTE_AANMANING,enum=NIO_WET_FINANCIERING,enum=NIO_WET_ABC_LENING,enum=NIO_WET_ABC_ONTHEFFING"`
}

func GetFinancieleZaakNaam(financieleVerplichtingType systemModel.FinancieleVerplichtingType) (string, error) {
	switch financieleVerplichtingType {
	case DUO_WET_STUDIEFINANCIERING:
		return "Studieschuld", nil
	case DUO_WET_INBURGERING_LENING:
		return "Lening inburgering", nil
	case DUO_WET_INBURGERING_MEDISCHE_ONTHEFFING:
		return "Kosten medische ontheffing", nil
	case DUO_OV_BOETE:
		return "OV-Boete", nil
	case CJIB_WAHV:
		return "Verkeersboete", nil
	case CJIB_ADMINISTRATIEKOSTEN:
		return "Administratiekosten", nil
	case CJIB_KOSTEN_EERSTE_AANMANING:
		return "Kosten eerste aanmaning", nil
	case CJIB_CAK_BESTUURSRECHTELIJKE_PREMIE_ZVW:
		return "Bestuursrechtelijke premie ZVW", nil
	case BD_INKOMENSHEFFING:
		return "Heffing inkomstenbelasting", nil
	case BD_BELASTINGRENTE:
		return "Belastingrente", nil
	case BD_MOTORRIJTUIGENBELASTING:
		return "Motorrijtuigenbelasting", nil
	case BD_ZORGVERZEKERINGSWET:
		return "Premie zorgverzekeringswet", nil
	case BD_ZORGVERZEKERINGSWET_BIJDRAGE:
		return "Inkomensafhankelijke bijdrage zorgverzekeringswet", nil
	case BD_ZORGTOESLAG:
		return "Zorgtoeslag", nil
	case GEMEENTE_OZB_EIGENAAR:
		return "Gemeentebelastingen", nil
	case GEMEENTE_RIOOLHEFFING:
		return "Gemeentebelastingen", nil
	case GEMEENTE_AFVALSTOFFENHEFFING:
		return "Gemeentebelastingen", nil
	case RVO_DGF:
		return "Dierengezondheidsfonds", nil
	case RVO_BOETE:
		return "Boete", nil
	case RVO_FOUT_BETAALORGAAN:
		return "Fout betaalorgaan", nil
	case RVO_HEFFING:
		return "Heffing", nil
	case RVO_MEERJAREN_SANCTIE:
		return "Meerjaren sanctie", nil
	case RVO_ONREGELMATIGHEID:
		return "Onregelmatigheid", nil
	case RVO_RANDVOORWAARDE:
		return "Randvoorwaarde", nil
	case RVO_SANCTIE:
		return "Sanctie", nil
	case RVO_TUSSENTIJDSE_VORDERING:
		return "Tussentijdse vordering", nil
	case RVO_VASTSTELLING:
		return "Vaststelling", nil
	case RVO_VORDERING:
		return "Vordering", nil
	case RVO_WETTELIJKE_RENTE:
		return "Wettelijke rente", nil
	case CAK_WLZ:
		return "Eigen bijdrage WLZ", nil
	case CAK_WMO:
		return "Eigen bijdrage WMO", nil
	case CAK_ZVW_WAN:
		return "Zorgverzekeringswet wanbetalersregeling", nil
	case CJIB_BUITENGERECHTELIJKE_AFDOENING_VAN_STRAFBARE_FEITEN:
		return "Buitengerechtelijke afdoening van strafbare feiten", nil
	case CJIB_OVERHEIDSVORDERING:
		return "Overheidsvordering", nil
	case CJIB_OVERHEIDSVORDERING_INNEN_EN_INCASSO:
		return "Overheidsvorderingen Innen en Incasso", nil
	case CJIB_TRANSACTIEVOORSTEL:
		return "Transactievoorstel", nil
	case CJIB_EUROPESE_GELDELIJKE_SANCTIE:
		return "Europese geldelijke sanctie", nil
	case CJIB_WAHV_BESCHIKKING:
		return "WAHV Beschikking", nil
	case CJIB_WAHV_BESCHIKKINGEN_MULDER_BOETES:
		return "WAHV beschikkingen (Mulder boetes)", nil
	case CJIB_WAARBORGSOM_SCHORSING_VOORLOPIGE_HECHTENIS:
		return "Waarborgsom schorsing voorlopige hechtenis", nil
	case CJIB_VOORWAARDELIJK_SEPOT_SCHADEVERGOEDING:
		return "Voorwaardelijk sepot schadevergoeding", nil
	case CJIB_SCHADEVERGOEDING_ALS_BIJZONDERE_VOORWAARDE:
		return "Schadevergoeding als bijzondere voorwaarde", nil
	case CJIB_BOETEVONNISSEN_STRABIS:
		return "Boetevonnissen (STRABIS)", nil
	case CJIB_WAARBORGSOM:
		return "Waarborgsom", nil
	case CJIB_STORTING_SCHADEFONDS:
		return "Storting Schadefonds", nil
	case CJIB_GELDSOM_VOORWAARDELIJKE_GRATIE:
		return "Geldsom voorwaardelijke gratie", nil
	case CJIB_CONFISCATIEBESLISSINGEN:
		return "Confiscatiebeslissingen", nil
	case CJIB_SCHIKKINGEN:
		return "Schikkingen", nil
	case CJIB_OM_AFDOENING:
		return "OM-afdoening", nil
	case CJIB_ONTNEMINGSMAATREGELEN_PLUKZE:
		return "Ontnemingsmaatregelen (PLUKZE)", nil
	case CJIB_SCHADEVERGOEDINGSMAATREGELEN_VONNISSEN:
		return "Schadevergoedingsmaatregelen vonnissen", nil
	case RMB_HEFFING:
		return "Heffing belasting", nil
	case RMB_FONDS_HEFFING:
		return "Heffing fonds", nil
	case RCI_BOETE:
		return "Boete overtreding", nil
	case RCI_ADMINISTRATIEKOSTEN:
		return "Administratiekosten", nil
	case RCI_KOSTEN_EERSTE_AANMANING:
		return "Kosten eerste aanmaning", nil
	case NIO_WET_FINANCIERING:
		return "Schuld wet financiering", nil
	case NIO_WET_ABC_LENING:
		return "Lening wet ABC", nil
	case NIO_WET_ABC_ONTHEFFING:
		return "Kosten ABC ontheffing", nil
	default:
		return "", fmt.Errorf("financieleVerplichtingType not found")
	}
}

func GetGebeurtenisNaam(financieleVerplichtingType systemModel.FinancieleVerplichtingType) (string, error) {
	switch financieleVerplichtingType {
	case DUO_WET_STUDIEFINANCIERING:
		return "Studieschuld", nil
	case DUO_WET_INBURGERING_LENING:
		return "Lening inburgering", nil
	case DUO_WET_INBURGERING_MEDISCHE_ONTHEFFING:
		return "Kosten medische ontheffing", nil
	case DUO_OV_BOETE:
		return "OV-Boete", nil
	case CJIB_WAHV:
		return "Verkeersboete", nil
	case CJIB_ADMINISTRATIEKOSTEN:
		return "Administratiekosten", nil
	case CJIB_KOSTEN_EERSTE_AANMANING:
		return "Kosten eerste aanmaning", nil
	case CJIB_CAK_BESTUURSRECHTELIJKE_PREMIE_ZVW:
		return "Bestuursrechtelijke premie ZVW", nil
	case BD_INKOMENSHEFFING:
		return "Heffing inkomstenbelasting", nil
	case BD_BELASTINGRENTE:
		return "Belastingrente", nil
	case BD_MOTORRIJTUIGENBELASTING:
		return "Motorrijtuigenbelasting", nil
	case BD_ZORGVERZEKERINGSWET:
		return "Premie zorgverzekeringswet", nil
	case BD_ZORGVERZEKERINGSWET_BIJDRAGE:
		return "Inkomensafhankelijke bijdrage zorgverzekeringswet", nil
	case BD_ZORGTOESLAG:
		return "Zorgtoeslag", nil
	case GEMEENTE_OZB_EIGENAAR:
		return "OZB eigenaar", nil
	case GEMEENTE_RIOOLHEFFING:
		return "Rioolheffing eigenaar", nil
	case GEMEENTE_AFVALSTOFFENHEFFING:
		return "Afvalstoffenheffing", nil
	case RVO_DGF:
		return "Aanslag Dierengezondheidsfonds", nil
	case RVO_BOETE:
		return "Opgelegde boete", nil
	case RVO_FOUT_BETAALORGAAN:
		return "Fout betaalorgaan", nil
	case RVO_HEFFING:
		return "Heffing", nil
	case RVO_MEERJAREN_SANCTIE:
		return "Meerjaren sanctie", nil
	case RVO_ONREGELMATIGHEID:
		return "Onregelmatigheid", nil
	case RVO_RANDVOORWAARDE:
		return "Randvoorwaarde", nil
	case RVO_SANCTIE:
		return "Opgelegde sanctie", nil
	case RVO_TUSSENTIJDSE_VORDERING:
		return "Opgelegde tussentijdse vordering", nil
	case RVO_VASTSTELLING:
		return "Vaststelling", nil
	case RVO_VORDERING:
		return "Opgelegde vordering", nil
	case RVO_WETTELIJKE_RENTE:
		return "Berekende wettelijke rente", nil
	case CAK_WLZ:
		return "Eigen bijdrage WLZ", nil
	case CAK_WMO:
		return "Eigen bijdrage WMO", nil
	case CAK_ZVW_WAN:
		return "Zorgverzekeringswet wanbetalersregeling", nil
	case CJIB_BUITENGERECHTELIJKE_AFDOENING_VAN_STRAFBARE_FEITEN:
		return "Buitengerechtelijke afdoening van strafbare feiten", nil
	case CJIB_OVERHEIDSVORDERING:
		return "Overheidsvordering", nil
	case CJIB_OVERHEIDSVORDERING_INNEN_EN_INCASSO:
		return "Overheidsvorderingen Innen en Incasso", nil
	case CJIB_TRANSACTIEVOORSTEL:
		return "Transactievoorstel", nil
	case CJIB_EUROPESE_GELDELIJKE_SANCTIE:
		return "Europese geldelijke sanctie", nil
	case CJIB_WAHV_BESCHIKKING:
		return "WAHV Beschikking", nil
	case CJIB_WAHV_BESCHIKKINGEN_MULDER_BOETES:
		return "WAHV beschikkingen (Mulder boetes)", nil
	case CJIB_WAARBORGSOM_SCHORSING_VOORLOPIGE_HECHTENIS:
		return "Waarborgsom schorsing voorlopige hechtenis", nil
	case CJIB_VOORWAARDELIJK_SEPOT_SCHADEVERGOEDING:
		return "Voorwaardelijk sepot schadevergoeding", nil
	case CJIB_SCHADEVERGOEDING_ALS_BIJZONDERE_VOORWAARDE:
		return "Schadevergoeding als bijzondere voorwaarde", nil
	case CJIB_BOETEVONNISSEN_STRABIS:
		return "Boetevonnissen (STRABIS)", nil
	case CJIB_WAARBORGSOM:
		return "Waarborgsom", nil
	case CJIB_STORTING_SCHADEFONDS:
		return "Storting Schadefonds", nil
	case CJIB_GELDSOM_VOORWAARDELIJKE_GRATIE:
		return "Geldsom voorwaardelijke gratie", nil
	case CJIB_CONFISCATIEBESLISSINGEN:
		return "Confiscatiebeslissingen", nil
	case CJIB_SCHIKKINGEN:
		return "Schikkingen", nil
	case CJIB_OM_AFDOENING:
		return "OM-afdoening", nil
	case CJIB_ONTNEMINGSMAATREGELEN_PLUKZE:
		return "Ontnemingsmaatregelen (PLUKZE)", nil
	case CJIB_SCHADEVERGOEDINGSMAATREGELEN_VONNISSEN:
		return "Schadevergoedingsmaatregelen vonnissen", nil
	case RMB_HEFFING:
		return "Heffing nelasting", nil
	case RMB_FONDS_HEFFING:
		return "Aanslag heffing fonds", nil
	case RCI_BOETE:
		return "Boete overtreding", nil
	case RCI_ADMINISTRATIEKOSTEN:
		return "Administratiekosten", nil
	case RCI_KOSTEN_EERSTE_AANMANING:
		return "Kosten eerste aanmaning", nil
	case NIO_WET_FINANCIERING:
		return "Schuld wet financiering", nil
	case NIO_WET_ABC_LENING:
		return "Lening wet ABC", nil
	case NIO_WET_ABC_ONTHEFFING:
		return "Kosten ABC ontheffing", nil
	default:
		return "", fmt.Errorf("financieleVerplichtingType not found")
	}
}
