// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

type Gebeurtenis struct {
	Type   string
	Fields []GebeurtenisField
}

type GebeurtenisField struct {
	Name        string
	Description string
}
