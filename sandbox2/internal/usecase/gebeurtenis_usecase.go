// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package usecases

import (
	"encoding/json"
	"html/template"
	"os"
	"path"
	"path/filepath"
	"reflect"
	"sort"

	"github.com/invopop/jsonschema"
	"gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox/sandbox2/internal/model"
	pkgModel "gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox/sandbox2/pkg/model"
	systemModel "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"
	"go.uber.org/zap"
)

type GebeurtenisUsecase struct {
	Logger *zap.Logger
}

func NewGebeurtenisUsecase(
	logger *zap.Logger,
) *GebeurtenisUsecase {
	return &GebeurtenisUsecase{
		Logger: logger,
	}
}

func (uc *GebeurtenisUsecase) GenerateSchema() error {
	reflector := new(jsonschema.Reflector)
	reflector.BaseSchemaID = "https://vorijk.nl/schema"

	// reflector.KeyNamer = strcase.LowerCamelCase

	if err := reflector.AddGoComments("gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox/sandbox2", "./"); err != nil {
		return err
	}

	reflectedEvents := []*jsonschema.Schema{}

	reflectedEvents = append(reflectedEvents, reflector.ReflectFromType(reflect.TypeOf(&pkgModel.FinancieleVerplichtingOpgelegd{})))

	reflectedEvents = append(reflectedEvents, reflector.ReflectFromType(reflect.TypeOf(&systemModel.FinancieleVerplichtingKwijtgescholden{})))
	reflectedEvents = append(reflectedEvents, reflector.ReflectFromType(reflect.TypeOf(&systemModel.FinancieleVerplichtingGecorrigeerd{})))
	reflectedEvents = append(reflectedEvents, reflector.ReflectFromType(reflect.TypeOf(&systemModel.BetalingsverplichtingOpgelegd{})))
	reflectedEvents = append(reflectedEvents, reflector.ReflectFromType(reflect.TypeOf(&systemModel.BetalingsverplichtingIngetrokken{})))
	reflectedEvents = append(reflectedEvents, reflector.ReflectFromType(reflect.TypeOf(&systemModel.BetalingVerwerkt{})))
	reflectedEvents = append(reflectedEvents, reflector.ReflectFromType(reflect.TypeOf(&systemModel.FinancieelRechtVastgesteld{})))
	reflectedEvents = append(reflectedEvents, reflector.ReflectFromType(reflect.TypeOf(&systemModel.BedragUitbetaald{})))
	reflectedEvents = append(reflectedEvents, reflector.ReflectFromType(reflect.TypeOf(&systemModel.VerrekeningVerwerkt{})))
	reflectedEvents = append(reflectedEvents, reflector.ReflectFromType(reflect.TypeOf(&systemModel.FinancieleZaakOvergedragen{})))
	reflectedEvents = append(reflectedEvents, reflector.ReflectFromType(reflect.TypeOf(&systemModel.FinancieleZaakOvergenomen{})))
	reflectedEvents = append(reflectedEvents, reflector.ReflectFromType(reflect.TypeOf(&systemModel.FinancieleZaakOvergedragenAanDeurwaarder{})))

	schema := jsonschema.Schema{
		Version:     "https://json-schema.org/draft/2020-12/schema",
		ID:          "https://vorijk.nl/schema",
		Definitions: jsonschema.Definitions{},
		Items: &jsonschema.Schema{
			OneOf: []*jsonschema.Schema{},
		},
		Type: "array",
	}

	for _, reflectedEvent := range reflectedEvents {
		schema.Items.OneOf = append(schema.Items.OneOf, &jsonschema.Schema{Ref: reflectedEvent.Ref})
	}

	for _, reflectedEvent := range reflectedEvents {
		for key, definition := range reflectedEvent.Definitions {
			schema.Definitions[key] = definition
		}
	}

	bytes, err := schema.MarshalJSON()
	if err != nil {
		return err
	}

	err = os.WriteFile("dist/gebeurtenis.schema.json", bytes, 0644)
	if err != nil {
		return err
	}

	return nil
}

func (uc *GebeurtenisUsecase) GenerateMarkdown() error {
	templatePath := filepath.Join("template/markdown", "gebeurtenissen.template.md")
	fileTemplatePath := path.Join(templatePath)

	templateFile, err := template.ParseFiles(fileTemplatePath)
	if err != nil {
		return err
	}

	outputFile, err := os.Create("dist/gebeurtenissen.md")
	if err != nil {
		return err
	}
	defer outputFile.Close()

	schemaBytes, err := os.ReadFile("dist/gebeurtenis.schema.json")
	if err != nil {
		return err
	}

	schema := jsonschema.Schema{}

	err = json.Unmarshal(schemaBytes, &schema)
	if err != nil {
		return err
	}

	gebeurtenisSlice := []model.Gebeurtenis{}
	for gebeurtenisType, definition := range schema.Definitions {
		fields := []model.GebeurtenisField{}

		for e := definition.Properties.Oldest(); e != nil; e = e.Next() {
			fields = append(fields, model.GebeurtenisField{
				Name:        e.Key,
				Description: e.Value.Description,
			})
		}

		gebeurtenisSlice = append(gebeurtenisSlice, model.Gebeurtenis{
			Type:   gebeurtenisType,
			Fields: fields,
		})
	}

	sort.Slice(gebeurtenisSlice, func(i, j int) bool {
		return gebeurtenisSlice[i].Type < gebeurtenisSlice[j].Type
	})

	for _, gebeurtenis := range gebeurtenisSlice {
		sort.Slice(gebeurtenis.Fields, func(i, j int) bool {
			return gebeurtenis.Fields[i].Name < gebeurtenis.Fields[j].Name
		})
	}

	err = templateFile.Execute(
		outputFile,
		gebeurtenisSlice,
	)
	if err != nil {
		return err
	}

	return nil
}
