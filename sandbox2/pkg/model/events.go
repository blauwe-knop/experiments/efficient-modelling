package model

import (
	"time"

	sandbox1Model "gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox/sandbox1/pkg/model"
	systemModel "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"
)

func (e FinancieleVerplichtingOpgelegd) IsEvent() {}
func (e FinancieleVerplichtingOpgelegd) GetBaseEvent() systemModel.BaseEvent {
	return e.BaseEvent
}

type FinancieleVerplichtingOpgelegd struct {
	DatumtijdOpgelegd               time.Time               `json:"datumtijd_opgelegd"`                  // datum en tijd waarop de financiële verplichting is opgelegd
	Zaakkenmerk                     systemModel.Zaakkenmerk `json:"zaakkenmerk"`                         // een kenmerk van de groep financiële verplichtingen waaraan deze verplichting gerelateerd is
	Beschikkingsnummer              string                  `json:"beschikkingsnummer,omitempty"`        // nummer van de beschikking behorend bij de financiële verplichting
	Bsn                             systemModel.Bsn         `json:"bsn"`                                 // het bsn van de persoon aan wie de verplichting is opgelegd
	PrimaireVerplichting            bool                    `json:"primaire_verplichting"`               // boolean die aangeeft of deze financiële verplichting de primaire financiële verplichting in een groep financiële verplichtingen is
	Categorie                       string                  `json:"categorie"`                           // een optionele aanduiding van de categorie van de verplichting
	Bedrag                          int                     `json:"bedrag"`                              // positief bedrag in eurocenten
	Omschrijving                    string                  `json:"omschrijving"`                        // leesbare versie van de juridische grondslag
	JuridischeGrondslagOmschrijving string                  `json:"juridische_grondslag_omschrijving"`   // Wetsartikel op basis waarvan de verplichting is opgelegd
	JuridischeGrondslagBron         string                  `json:"juridische_grondslag_bron,omitempty"` // een deeplink naar wetten.overheid.nl of andere wetsbeschrijving
	OpgelegdDoor                    string                  `json:"opgelegd_door"`                       // organisatie OIN
	UitgevoerdDoor                  string                  `json:"uitgevoerd_door"`                     // organisatie OIN
	sandbox1Model.EventType
	systemModel.BaseEvent
}
