# Gebeurtenissen

In dit document staan de verschillende gebeurtenissen binnen het Vorderingenoverzicht Rijk beschreven.

{{range .}} ## {{ .Type }}

| Veld | Omschrijving |
| --- | --- |
{{- range .Fields }}
| {{ .Name }} | {{ .Description }} |
{{- end}}
{{end}}
