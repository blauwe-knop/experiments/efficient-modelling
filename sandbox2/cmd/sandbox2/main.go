// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package main

import (
	"fmt"
	"log"

	usecases "gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox/sandbox2/internal/usecase"
	"go.uber.org/zap"
)

type options struct {
	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	logger, err := newZapLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	gebeurtenisUsecase := usecases.NewGebeurtenisUsecase(
		logger,
	)

	err = gebeurtenisUsecase.GenerateSchema()
	if err != nil {
		logger.Fatal("--- Gebeurtenis GenerateSchema: FAILED ---", zap.Error(err))
	} else {
		logger.Info("--- Gebeurtenis GenerateSchema: SUCCEEDED ---")
	}

	err = gebeurtenisUsecase.GenerateMarkdown()
	if err != nil {
		logger.Fatal("--- Gebeurtenis GenerateMarkdown: FAILED ---", zap.Error(err))
	} else {
		logger.Info("--- Gebeurtenis GenerateMarkdown: SUCCEEDED ---")
	}

	//TODO: generate dart code for events
}

func newZapLogger(logOptions LogOptions) (*zap.Logger, error) {
	config := logOptions.ZapConfig()
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return logger, nil
}
