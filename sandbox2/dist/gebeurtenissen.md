# Gebeurtenissen

In dit document staan de verschillende gebeurtenissen binnen het Vorderingenoverzicht Rijk beschreven.

## BedragUitbetaald

| Veld                  | Omschrijving |
| --------------------- | ------------ |
| bedrag                |              |
| bsn                   |              |
| datumtijd_gebeurtenis |              |
| gebeurtenis_kenmerk   |              |
| gebeurtenis_type      |              |
| omschrijving          |              |
| rekeningnummer        |              |
| uitgevoerd_door       |              |
| zaakkenmerk           |              |

## BetalingVerwerkt

| Veld                  | Omschrijving |
| --------------------- | ------------ |
| bedrag                |              |
| betalingskenmerk      |              |
| datumtijd_gebeurtenis |              |
| datumtijd_ontvangen   |              |
| datumtijd_verwerkt    |              |
| gebeurtenis_kenmerk   |              |
| gebeurtenis_type      |              |
| ontvangen_door        |              |
| verwerkt_door         |              |

## BetalingsverplichtingIngetrokken

| Veld                            | Omschrijving |
| ------------------------------- | ------------ |
| datumtijd_gebeurtenis           |              |
| gebeurtenis_kenmerk             |              |
| gebeurtenis_type                |              |
| ingetrokken_gebeurtenis_kenmerk |              |

## BetalingsverplichtingOpgelegd

| Veld                          | Omschrijving |
| ----------------------------- | ------------ |
| bedrag                        |              |
| betaalwijze                   |              |
| betalingskenmerk              |              |
| bsn                           |              |
| datumtijd_gebeurtenis         |              |
| datumtijd_opgelegd            |              |
| gebeurtenis_kenmerk           |              |
| gebeurtenis_type              |              |
| omschrijving                  |              |
| rekeningnummer                |              |
| rekeningnummer_tenaamstelling |              |
| te_betalen_aan                |              |
| type                          |              |
| vervaldatum                   |              |
| zaakkenmerk                   |              |

## FinancieelRechtVastgesteld

| Veld                              | Omschrijving |
| --------------------------------- | ------------ |
| bedrag                            |              |
| bsn                               |              |
| datumtijd_gebeurtenis             |              |
| gebeurtenis_kenmerk               |              |
| gebeurtenis_type                  |              |
| juridische_grondslag_bron         |              |
| juridische_grondslag_omschrijving |              |
| omschrijving                      |              |
| uitgevoerd_door                   |              |
| zaakkenmerk                       |              |

## FinancieleVerplichtingGecorrigeerd

| Veld                                          | Omschrijving |
| --------------------------------------------- | ------------ |
| bsn                                           |              |
| datumtijd_gebeurtenis                         |              |
| gebeurtenis_kenmerk                           |              |
| gebeurtenis_type                              |              |
| juridische_grondslag_bron                     |              |
| juridische_grondslag_omschrijving             |              |
| kenmerk_gecorrigeerde_financiele_verplichting |              |
| nieuw_opgelegd_bedrag                         |              |
| omschrijving                                  |              |
| uitgevoerd_door                               |              |
| zaakkenmerk                                   |              |

## FinancieleVerplichtingKwijtgescholden

| Veld                                            | Omschrijving |
| ----------------------------------------------- | ------------ |
| bedrag_kwijtschelding                           |              |
| bsn                                             |              |
| datumtijd_gebeurtenis                           |              |
| gebeurtenis_kenmerk                             |              |
| gebeurtenis_type                                |              |
| juridische_grondslag_bron                       |              |
| juridische_grondslag_omschrijving               |              |
| kenmerk_kwijtgescholden_financiele_verplichting |              |
| omschrijving                                    |              |
| uitgevoerd_door                                 |              |
| zaakkenmerk                                     |              |

## FinancieleVerplichtingOpgelegd

| Veld                              | Omschrijving                                                                                                                       |
| --------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------- |
| bedrag                            | positief bedrag in eurocenten                                                                                                      |
| beschikkingsnummer                | nummer van de beschikking behorend bij de financiële verplichting                                                                  |
| bsn                               | het bsn van de persoon aan wie de verplichting is opgelegd                                                                         |
| categorie                         | een optionele aanduiding van de categorie van de verplichting                                                                      |
| datumtijd_gebeurtenis             |                                                                                                                                    |
| datumtijd_opgelegd                | datum en tijd waarop de financiële verplichting is opgelegd                                                                        |
| gebeurtenis_kenmerk               |                                                                                                                                    |
| gebeurtenis_type                  |                                                                                                                                    |
| juridische_grondslag_bron         | een deeplink naar wetten.overheid.nl of andere wetsbeschrijving                                                                    |
| juridische_grondslag_omschrijving | Wetsartikel op basis waarvan de verplichting is opgelegd                                                                           |
| omschrijving                      | leesbare versie van de juridische grondslag                                                                                        |
| opgelegd_door                     | organisatie OIN                                                                                                                    |
| primaire_verplichting             | boolean die aangeeft of deze financiële verplichting de primaire financiële verplichting in een groep financiële verplichtingen is |
| type                              |                                                                                                                                    |
| uitgevoerd_door                   | organisatie OIN                                                                                                                    |
| zaakkenmerk                       | een kenmerk van de groep financiële verplichtingen waaraan deze verplichting gerelateerd is                                        |

## FinancieleZaakOvergedragen

| Veld                              | Omschrijving |
| --------------------------------- | ------------ |
| bsn                               |              |
| datumtijd_gebeurtenis             |              |
| gebeurtenis_kenmerk               |              |
| gebeurtenis_type                  |              |
| juridische_grondslag_bron         |              |
| juridische_grondslag_omschrijving |              |
| omschrijving                      |              |
| overgedragen_aan                  |              |
| saldo_bij_overdracht              |              |
| uitgevoerd_door                   |              |
| zaakkenmerk                       |              |

## FinancieleZaakOvergedragenAanDeurwaarder

| Veld                              | Omschrijving |
| --------------------------------- | ------------ |
| bsn                               |              |
| datumtijd_gebeurtenis             |              |
| email_adres_deurwaarder_url       |              |
| gebeurtenis_kenmerk               |              |
| gebeurtenis_type                  |              |
| juridische_grondslag_bron         |              |
| juridische_grondslag_omschrijving |              |
| naam_deurwaarder                  |              |
| omschrijving                      |              |
| overgedragen_aan                  |              |
| saldo_bij_overdracht              |              |
| telefoonnummer_deurwaarder_url    |              |
| uitgevoerd_door                   |              |
| zaakkenmerk                       |              |

## FinancieleZaakOvergenomen

| Veld                              | Omschrijving |
| --------------------------------- | ------------ |
| bsn                               |              |
| datumtijd_gebeurtenis             |              |
| gebeurtenis_kenmerk               |              |
| gebeurtenis_type                  |              |
| juridische_grondslag_bron         |              |
| juridische_grondslag_omschrijving |              |
| omschrijving                      |              |
| overgenomen_van                   |              |
| saldo_bij_overdracht              |              |
| uitgevoerd_door                   |              |
| zaakkenmerk                       |              |

## VerrekeningVerwerkt

| Veld                              | Omschrijving |
| --------------------------------- | ------------ |
| bedrag_verrekening                |              |
| bsn                               |              |
| datumtijd_gebeurtenis             |              |
| gebeurtenis_kenmerk               |              |
| gebeurtenis_type                  |              |
| juridische_grondslag_bron         |              |
| juridische_grondslag_omschrijving |              |
| kenmerk_financiele_verplichting   |              |
| omschrijving                      |              |
| uitgevoerd_door                   |              |
| zaakkenmerk                       |              |
