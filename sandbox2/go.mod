module gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox/sandbox2

go 1.22.3

require (
	github.com/invopop/jsonschema v0.12.0
	github.com/jessevdk/go-flags v1.6.1
	github.com/thessem/zap-prettyconsole v0.5.0
	gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox/sandbox1 v0.0.0
	gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system v0.19.6
	go.uber.org/zap v1.27.0
)

require (
	github.com/Code-Hex/dd v1.1.0 // indirect
	github.com/bahlo/generic-list-go v0.2.0 // indirect
	github.com/buger/jsonparser v1.1.1 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/wk8/go-ordered-map/v2 v2.1.8 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/sys v0.22.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

replace gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox/sandbox1 => ../sandbox1
