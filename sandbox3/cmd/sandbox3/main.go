// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package main

import (
	"fmt"
	"log"

	"gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox/sandbox3/internal/model"
	usecases "gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox/sandbox3/internal/usecase"
	"go.uber.org/zap"
)

type options struct {
	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	logger, err := newZapLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	scenarioUsecase := usecases.NewScenarioUsecase(
		logger,
	)

	err = scenarioUsecase.GenerateSchema()
	if err != nil {
		logger.Fatal("--- Scenario GenerateSchema: FAILED ---", zap.Error(err))
	} else {
		logger.Info("--- Scenario GenerateSchema: SUCCEEDED ---")
	}

	scenarioFilenames, err := scenarioUsecase.GetAllScenarioFilenames()
	if err != nil {
		logger.Fatal("--- Scenario GetAllscenarioFilenames: FAILED ---", zap.Error(err))
	} else {
		logger.Info("--- Scenario GetAllscenarioFilenames: SUCCEEDED ---")
	}

	demoData := []model.IndexOrganization{}
	for _, scenarioFilename := range *scenarioFilenames {
		yamlData, err := scenarioUsecase.ValidateAndUnmarshalYaml(scenarioFilename)
		if err != nil {
			logger.Fatal("--- Scenario ValidateAndUnmarshalYaml: FAILED ---", zap.String("scenarioFilename", scenarioFilename), zap.Error(err))
		} else {
			logger.Info("--- Scenario ValidateAndUnmarshalYaml: SUCCEEDED ---", zap.String("scenarioFilename", scenarioFilename))
		}

		financialClaimsInformationDocumentMap, err := scenarioUsecase.ConvertYamlDataToFinancialClaimsInformationDocumentMap(*yamlData)
		if err != nil {
			logger.Fatal("--- Scenario ConvertYamlDataToFinancialClaimsInformationDocumentMap: FAILED ---", zap.String("scenarioFilename", scenarioFilename), zap.Error(err))
		} else {
			logger.Info("--- Scenario ConvertYamlDataToFinancialClaimsInformationDocumentMap: SUCCEEDED ---", zap.String("scenarioFilename", scenarioFilename))
		}

		err = scenarioUsecase.GeneratePlantuml(*financialClaimsInformationDocumentMap)
		if err != nil {
			logger.Fatal("--- Scenario GeneratePlantuml: FAILED ---", zap.String("scenarioFilename", scenarioFilename), zap.Error(err))
		} else {
			logger.Info("--- Scenario GeneratePlantuml: SUCCEEDED ---", zap.String("scenarioFilename", scenarioFilename))
		}

		err = scenarioUsecase.FinancialClaimsInformationDocumentToJson(*financialClaimsInformationDocumentMap)
		if err != nil {
			logger.Fatal("--- Scenario FinancialClaimsInformationDocumentToJson: FAILED ---", zap.String("scenarioFilename", scenarioFilename), zap.Error(err))
		} else {
			logger.Info("--- Scenario FinancialClaimsInformationDocumentToJson: SUCCEEDED ---", zap.String("scenarioFilename", scenarioFilename))
		}

		err = scenarioUsecase.GebeurtenissenToJson(&demoData, *financialClaimsInformationDocumentMap)
		if err != nil {
			logger.Fatal("--- Scenario ScenariosenToJson: FAILED ---", zap.String("scenarioFilename", scenarioFilename), zap.Error(err))
		} else {
			logger.Info("--- Scenario ScenariosenToJson: SUCCEEDED ---", zap.String("scenarioFilename", scenarioFilename))
		}

		err = scenarioUsecase.CreateDemoIndexFile(&demoData)
		if err != nil {
			logger.Fatal("--- Scenario CreateDemoIndexFile: FAILED ---", zap.Reflect("demoData", demoData), zap.Error(err))
		} else {
			logger.Info("--- Scenario CreateDemoIndexFile: SUCCEEDED ---")
		}
	}

}

func newZapLogger(logOptions LogOptions) (*zap.Logger, error) {
	config := logOptions.ZapConfig()
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return logger, nil
}
