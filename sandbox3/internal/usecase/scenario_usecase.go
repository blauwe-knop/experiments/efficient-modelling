// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package usecases

import (
	"encoding/json"
	"fmt"
	"html/template"
	"os"
	"path"
	"path/filepath"
	"reflect"
	"slices"
	"sort"
	"strings"
	"time"

	"github.com/ghodss/yaml"
	"github.com/invopop/jsonschema"
	"github.com/xeipuuv/gojsonschema"
	sandbox1Model "gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox/sandbox1/pkg/model"
	sandbox2Model "gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox/sandbox2/pkg/model"
	"gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox/sandbox3/internal/model"
	demoSystemModel "gitlab.com/blauwe-knop/vorderingenoverzicht/demo-source-system/pkg/model"
	systemModel "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"
	"go.uber.org/zap"
)

type ScenarioUsecase struct {
	Logger *zap.Logger
}

func NewScenarioUsecase(
	logger *zap.Logger,
) *ScenarioUsecase {
	return &ScenarioUsecase{
		Logger: logger,
	}
}

func (uc *ScenarioUsecase) GenerateSchema() error {
	reflector := new(jsonschema.Reflector)
	reflector.BaseSchemaID = "https://vorijk.nl/schema"

	// reflector.KeyNamer = strcase.LowerCamelCase

	if err := reflector.AddGoComments("gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox/sandbox3", "./"); err != nil {
		return err
	}

	reflectedYamlData := reflector.ReflectFromType(reflect.TypeOf(&model.YamlData{}))
	reflectedScenario := reflector.ReflectFromType(reflect.TypeOf(&model.Scenario{}))

	reflectedEvents := []*jsonschema.Schema{}

	reflectedEvents = append(reflectedEvents, reflector.ReflectFromType(reflect.TypeOf(&sandbox2Model.FinancieleVerplichtingOpgelegd{})))

	reflectedEvents = append(reflectedEvents, reflector.ReflectFromType(reflect.TypeOf(&systemModel.FinancieleVerplichtingKwijtgescholden{})))
	reflectedEvents = append(reflectedEvents, reflector.ReflectFromType(reflect.TypeOf(&systemModel.FinancieleVerplichtingGecorrigeerd{})))
	reflectedEvents = append(reflectedEvents, reflector.ReflectFromType(reflect.TypeOf(&systemModel.BetalingsverplichtingOpgelegd{})))
	reflectedEvents = append(reflectedEvents, reflector.ReflectFromType(reflect.TypeOf(&systemModel.BetalingsverplichtingIngetrokken{})))
	reflectedEvents = append(reflectedEvents, reflector.ReflectFromType(reflect.TypeOf(&systemModel.BetalingVerwerkt{})))
	reflectedEvents = append(reflectedEvents, reflector.ReflectFromType(reflect.TypeOf(&systemModel.FinancieelRechtVastgesteld{})))
	reflectedEvents = append(reflectedEvents, reflector.ReflectFromType(reflect.TypeOf(&systemModel.BedragUitbetaald{})))
	reflectedEvents = append(reflectedEvents, reflector.ReflectFromType(reflect.TypeOf(&systemModel.VerrekeningVerwerkt{})))
	reflectedEvents = append(reflectedEvents, reflector.ReflectFromType(reflect.TypeOf(&systemModel.FinancieleZaakOvergedragen{})))
	reflectedEvents = append(reflectedEvents, reflector.ReflectFromType(reflect.TypeOf(&systemModel.FinancieleZaakOvergenomen{})))
	reflectedEvents = append(reflectedEvents, reflector.ReflectFromType(reflect.TypeOf(&systemModel.FinancieleZaakOvergedragenAanDeurwaarder{})))

	ss, ok := reflectedYamlData.Definitions["YamlData"].Properties.Get("gebeurtenissen")
	if !ok {
		return fmt.Errorf("reflectedYamlData gebeurtenissen property does not exist")
	}

	ss.Items.OneOf = []*jsonschema.Schema{}

	for _, reflectedEvent := range reflectedEvents {
		ss.Items.OneOf = append(ss.Items.OneOf, &jsonschema.Schema{Ref: reflectedEvent.Ref})
	}

	schema := jsonschema.Schema{
		Version:     "https://json-schema.org/draft/2020-12/schema",
		ID:          "https://vorijk.nl/schema",
		Definitions: jsonschema.Definitions{},
		Properties:  reflectedYamlData.Definitions["YamlData"].Properties,
		Type:        "object",
	}

	for key, definition := range reflectedScenario.Definitions {
		schema.Definitions[key] = definition
	}

	for _, reflectedEvent := range reflectedEvents {
		for key, definition := range reflectedEvent.Definitions {
			schema.Definitions[key] = definition
		}
	}

	bytes, err := schema.MarshalJSON()
	if err != nil {
		return err
	}

	err = os.WriteFile("dist/scenario.schema.json", bytes, 0644)
	if err != nil {
		return err
	}

	return nil
}

func (uc *ScenarioUsecase) GetAllScenarioFilenames() (*[]string, error) {
	scenarioFilenames := []string{}
	folderName := "scenarios"
	dirEntries, err := os.ReadDir(folderName)
	if err != nil {
		return nil, err
	}

	for _, dirEntry := range dirEntries {
		scenarioFilenames = append(scenarioFilenames, dirEntry.Name())
	}

	return &scenarioFilenames, nil
}

func (uc *ScenarioUsecase) ValidateAndUnmarshalYaml(scenarioFilename string) (*model.YamlData, error) {
	schemaPath := "dist/scenario.schema.json"

	schemaBytes, err := os.ReadFile(schemaPath)
	if err != nil {
		return nil, err
	}

	folderName := "scenarios"
	yamlDocumentBytes, err := os.ReadFile(fmt.Sprintf("%s/%s", folderName, scenarioFilename))
	if err != nil {
		return nil, err
	}

	jsonDocumentBytes, err := yaml.YAMLToJSON(yamlDocumentBytes)
	if err != nil {
		return nil, err
	}

	schemaLoader := gojsonschema.NewStringLoader(string(schemaBytes))

	documentLoader := gojsonschema.NewStringLoader(string(jsonDocumentBytes))

	validator, err := gojsonschema.Validate(schemaLoader, documentLoader)
	if err != nil {
		return nil, err
	}

	if !validator.Valid() {
		return nil, err
	}

	yamlData := model.YamlData{}
	err = json.Unmarshal(jsonDocumentBytes, &yamlData)
	if err != nil {
		return nil, err
	}

	fileNamePeriodIndex := strings.LastIndex(scenarioFilename, ".")

	fileNameWithoutExtension := scenarioFilename[:fileNamePeriodIndex]

	scenarioNames := make(map[int]string)
	for index, scenario := range yamlData.Scenarios {
		scenarioNames[index] = fmt.Sprintf("%s_%d_%s", fileNameWithoutExtension, index+1, scenario.Name)
	}

	for index, name := range scenarioNames {
		yamlData.Scenarios[index].Name = name
	}

	return &yamlData, nil
}

func (uc *ScenarioUsecase) ConvertYamlDataToFinancialClaimsInformationDocumentMap(yamlData model.YamlData) (*map[string]systemModel.FinancialClaimsInformationDocument, error) {
	financialClaimsInformationDocumentMap := make(map[string]systemModel.FinancialClaimsInformationDocument)

	for _, scenario := range yamlData.Scenarios {
		eventSliceByZaakkenmerk := make(map[systemModel.Zaakkenmerk][]systemModel.Event)
		achterstandenGebeurteniskenmerkenByZaakkenmerk := make(map[systemModel.Zaakkenmerk][]systemModel.GebeurtenisKenmerk)
		contactOptiesByZaakkenmerk := make(map[systemModel.Zaakkenmerk][]systemModel.ContactOptie)

		for _, scenarioAchterstand := range scenario.ScenarioAchterstanden {
			_, ok := achterstandenGebeurteniskenmerkenByZaakkenmerk[scenarioAchterstand.Zaakkenmerk]
			if !ok {
				achterstandenGebeurteniskenmerkenByZaakkenmerk[scenarioAchterstand.Zaakkenmerk] = []systemModel.GebeurtenisKenmerk{scenarioAchterstand.GebeurtenisKenmerk}
			} else {
				achterstandenGebeurteniskenmerkenByZaakkenmerk[scenarioAchterstand.Zaakkenmerk] = append(achterstandenGebeurteniskenmerkenByZaakkenmerk[scenarioAchterstand.Zaakkenmerk], scenarioAchterstand.GebeurtenisKenmerk)
			}
		}

		for _, scenarioContactOptie := range scenario.ScenarioContactOpties {
			_, ok := contactOptiesByZaakkenmerk[scenarioContactOptie.Zaakkenmerk]
			if !ok {
				contactOptiesByZaakkenmerk[scenarioContactOptie.Zaakkenmerk] = scenarioContactOptie.ContactOpties
			} else {
				contactOptiesByZaakkenmerk[scenarioContactOptie.Zaakkenmerk] = append(contactOptiesByZaakkenmerk[scenarioContactOptie.Zaakkenmerk], scenarioContactOptie.ContactOpties...)
			}
		}

		var bsn systemModel.Bsn

		for _, gebeurtenis := range yamlData.Gebeurtenissen {
			baseEvent := gebeurtenis.GetBaseEvent()
			if slices.Contains(scenario.GebeurtenisKenmerken, baseEvent.GebeurtenisKenmerk) {

				var zaakkenmerk systemModel.Zaakkenmerk

				switch baseEvent.GebeurtenisType {
				case "FinancieleVerplichtingOpgelegd":
					financieleVerplichtingOpgelegd, ok := gebeurtenis.(systemModel.FinancieleVerplichtingOpgelegd)
					if !ok {
						uc.Logger.Error("cast to FinancieleVerplichtingOpgelegd failed", zap.Reflect("gebeurtenis", gebeurtenis))
						return nil, fmt.Errorf("cast to FinancieleVerplichtingOpgelegd failed")
					}

					if financieleVerplichtingOpgelegd.PrimaireVerplichting {
						bsn = financieleVerplichtingOpgelegd.Bsn
					}

					zaakkenmerk = financieleVerplichtingOpgelegd.Zaakkenmerk

				case "FinancieleVerplichtingKwijtgescholden":
					financieleVerplichtingKwijtgescholden, ok := gebeurtenis.(systemModel.FinancieleVerplichtingKwijtgescholden)
					if !ok {
						return nil, fmt.Errorf("cast to FinancieleVerplichtingKwijtgescholden failed")
					}

					zaakkenmerk = financieleVerplichtingKwijtgescholden.Zaakkenmerk

				case "FinancieleVerplichtingGecorrigeerd":
					financieleVerplichtingGecorrigeerd, ok := gebeurtenis.(systemModel.FinancieleVerplichtingGecorrigeerd)
					if !ok {
						return nil, fmt.Errorf("cast to FinancieleVerplichtingGecorrigeerd failed")
					}

					zaakkenmerk = financieleVerplichtingGecorrigeerd.Zaakkenmerk

				case "BetalingsverplichtingOpgelegd":
					betalingsverplichtingOpgelegd, ok := gebeurtenis.(systemModel.BetalingsverplichtingOpgelegd)
					if !ok {
						return nil, fmt.Errorf("cast to BetalingsverplichtingOpgelegd failed")
					}
					zaakkenmerk = betalingsverplichtingOpgelegd.Zaakkenmerk

				case "BetalingsverplichtingIngetrokken":
					betalingsverplichtingIngetrokken, ok := gebeurtenis.(systemModel.BetalingsverplichtingIngetrokken)
					if !ok {
						return nil, fmt.Errorf("cast to BetalingsverplichtingIngetrokken failed")
					}

					for _, secondaryGebeurtenis := range yamlData.Gebeurtenissen {
						secondaryBaseEvent := secondaryGebeurtenis.GetBaseEvent()
						if secondaryBaseEvent.GebeurtenisType == "BetalingsverplichtingOpgelegd" && secondaryBaseEvent.GebeurtenisKenmerk == betalingsverplichtingIngetrokken.IngetrokkenGebeurtenisKenmerk {
							betalingsverplichtingOpgelegd, ok := secondaryGebeurtenis.(systemModel.BetalingsverplichtingOpgelegd)
							if !ok {
								return nil, fmt.Errorf("cast to BetalingsverplichtingOpgelegd failed")
							}
							zaakkenmerk = betalingsverplichtingOpgelegd.Zaakkenmerk
						}
					}

				case "BetalingVerwerkt":
					betalingVerwerkt, ok := gebeurtenis.(systemModel.BetalingVerwerkt)
					if !ok {
						return nil, fmt.Errorf("cast to BetalingVerwerkt failed")
					}

					for _, secondaryGebeurtenis := range yamlData.Gebeurtenissen {
						if secondaryGebeurtenis.GetBaseEvent().GebeurtenisType == "BetalingsverplichtingOpgelegd" {
							betalingsverplichtingOpgelegd, ok := secondaryGebeurtenis.(systemModel.BetalingsverplichtingOpgelegd)
							if !ok {
								return nil, fmt.Errorf("cast to BetalingsverplichtingOpgelegd failed")
							}

							if betalingsverplichtingOpgelegd.Betalingskenmerk == betalingVerwerkt.Betalingskenmerk {
								zaakkenmerk = betalingsverplichtingOpgelegd.Zaakkenmerk
							}
						}
					}

				case "FinancieelRechtVastgesteld":
					financieelRechtVastgesteld, ok := gebeurtenis.(systemModel.FinancieelRechtVastgesteld)
					if !ok {
						return nil, fmt.Errorf("cast to FinancieelRechtVastgesteld failed")
					}
					zaakkenmerk = financieelRechtVastgesteld.Zaakkenmerk

				case "BedragUitbetaald":
					bedragUitbetaald, ok := gebeurtenis.(systemModel.BedragUitbetaald)
					if !ok {
						return nil, fmt.Errorf("cast to BedragUitbetaald failed")
					}
					zaakkenmerk = bedragUitbetaald.Zaakkenmerk

				case "VerrekeningVerwerkt":
					verrekeningVerwerkt, ok := gebeurtenis.(systemModel.VerrekeningVerwerkt)
					if !ok {
						return nil, fmt.Errorf("cast to VerrekeningVerwerkt failed")
					}
					zaakkenmerk = verrekeningVerwerkt.Zaakkenmerk

				case "FinancieleZaakOvergedragen":
					financieleZaakOvergedragen, ok := gebeurtenis.(systemModel.FinancieleZaakOvergedragen)
					if !ok {
						return nil, fmt.Errorf("cast to FinancieleZaakOvergedragen failed")
					}
					zaakkenmerk = financieleZaakOvergedragen.Zaakkenmerk

				case "FinancieleZaakOvergenomen":
					financieleZaakOvergenomen, ok := gebeurtenis.(systemModel.FinancieleZaakOvergenomen)
					if !ok {
						return nil, fmt.Errorf("cast to FinancieleZaakOvergenomen failed")
					}
					zaakkenmerk = financieleZaakOvergenomen.Zaakkenmerk

				case "FinancieleZaakOvergedragenAanDeurwaarder":
					financieleZaakOvergedragenAanDeurwaarder, ok := gebeurtenis.(systemModel.FinancieleZaakOvergedragenAanDeurwaarder)
					if !ok {
						return nil, fmt.Errorf("cast to FinancieleZaakOvergedragenAanDeurwaarder failed")
					}
					zaakkenmerk = financieleZaakOvergedragenAanDeurwaarder.Zaakkenmerk

				}

				if len(zaakkenmerk) > 0 {
					_, ok := eventSliceByZaakkenmerk[zaakkenmerk]
					if !ok {
						eventSliceByZaakkenmerk[zaakkenmerk] = []systemModel.Event{gebeurtenis}
					} else {
						eventSliceByZaakkenmerk[zaakkenmerk] = append(eventSliceByZaakkenmerk[zaakkenmerk], gebeurtenis)
					}
				}
			}
		}

		financieleZaken := []systemModel.FinancialClaimsInformationDocumentFinancieleZaak{}

		for zaakkenmerk, events := range eventSliceByZaakkenmerk {
			financieleZaak, err := systemModel.NewFinancieleZaakFromEvents(events, achterstandenGebeurteniskenmerkenByZaakkenmerk[zaakkenmerk], contactOptiesByZaakkenmerk[zaakkenmerk])
			if err != nil {
				return nil, err
			}

			financieleZaken = append(financieleZaken, *financieleZaak)
		}

		beschikbaarheid := map[systemModel.FinancieleVerplichtingType]systemModel.BeschikbaarheidStatus{}
		for financieleVerplichtingType, beschikbaarheidStatus := range scenario.ScenarioBeschikbaarheid {
			beschikbaarheid[systemModel.FinancieleVerplichtingType(financieleVerplichtingType)] = beschikbaarheidStatus
		}

		financialClaimsInformationDocumentMap[scenario.Name] = systemModel.FinancialClaimsInformationDocument{
			Type:    "FINANCIAL_CLAIMS_INFORMATION_DOCUMENT",
			Version: "4",
			Body: systemModel.FinancialClaimsInformationDocumentBody{
				AangeleverdDoor:   scenario.AangeleverdDoor,
				DocumentDatumtijd: scenario.DocumentDatumtijd,
				Bsn:               bsn,
				FinancieleZaken:   financieleZaken,
				Beschikbaarheid:   beschikbaarheid,
			},
		}

	}

	return &financialClaimsInformationDocumentMap, nil
}

func (uc *ScenarioUsecase) GeneratePlantuml(financialClaimsInformationDocumentMap map[string]systemModel.FinancialClaimsInformationDocument) error {
	templatePath := filepath.Join("template/plantuml", "scenario.template.puml")
	fileTemplatePath := path.Join(templatePath)

	templateFile, err := template.ParseFiles(fileTemplatePath)
	if err != nil {
		return err
	}

	for name, financialClaimsInformationDocument := range financialClaimsInformationDocumentMap {
		for _, financieleZaak := range financialClaimsInformationDocument.Body.FinancieleZaken {
			fileName := name

			if len(financialClaimsInformationDocument.Body.FinancieleZaken) > 1 {
				fileName = fmt.Sprintf("%s-%s", name, financieleZaak.Zaakkenmerk)
			}

			financieleZaakNaam := ""
			for _, gebeurtenis := range financieleZaak.Gebeurtenissen {
				financieleVerplichtingOpgelegd, ok := gebeurtenis.(systemModel.FinancieleVerplichtingOpgelegd)

				if ok {
					if financieleVerplichtingOpgelegd.PrimaireVerplichting {
						financieleZaakNaam, err = sandbox1Model.GetFinancieleZaakNaam(financieleVerplichtingOpgelegd.Type)
						if err != nil {
							return err
						}
						break
					}
				}
			}

			outputFile, err := os.Create(fmt.Sprintf("dist/puml/%s.puml", fileName))
			if err != nil {
				return err
			}

			defer outputFile.Close()

			type TemplateData struct {
				Name               string
				DocumentDatumtijd  time.Time
				FinancieleZaakNaam string
				FinancieleZaak     systemModel.FinancialClaimsInformationDocumentFinancieleZaak
			}

			err = templateFile.Execute(
				outputFile,
				TemplateData{
					Name:               fileName,
					DocumentDatumtijd:  financialClaimsInformationDocument.Body.DocumentDatumtijd,
					FinancieleZaakNaam: financieleZaakNaam,
					FinancieleZaak:     financieleZaak,
				},
			)

			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (uc *ScenarioUsecase) FinancialClaimsInformationDocumentToJson(financialClaimsInformationDocumentMap map[string]systemModel.FinancialClaimsInformationDocument) error {
	for name, financialClaimsInformationDocument := range financialClaimsInformationDocumentMap {
		sort.Slice(financialClaimsInformationDocument.Body.FinancieleZaken, func(i, j int) bool {
			return financialClaimsInformationDocument.Body.FinancieleZaken[i].Zaakkenmerk < financialClaimsInformationDocument.Body.FinancieleZaken[j].Zaakkenmerk
		})

		outputFile, err := os.Create(fmt.Sprintf("dist/mock-json/%s.json", name))
		if err != nil {
			return err
		}

		defer outputFile.Close()

		bytes, err := json.Marshal(financialClaimsInformationDocument)
		if err != nil {
			return err
		}

		_, err = outputFile.Write(bytes)
		if err != nil {
			return err
		}
	}

	return nil
}

func (uc *ScenarioUsecase) GebeurtenissenToJson(demoData *[]model.IndexOrganization, financialClaimsInformationDocumentMap map[string]systemModel.FinancialClaimsInformationDocument) error {
	createdGebeurtenisKenmerkSlice := []systemModel.GebeurtenisKenmerk{}

	names := make([]string, 0, len(financialClaimsInformationDocumentMap))

	for key := range financialClaimsInformationDocumentMap {
		names = append(names, key)
	}

	sort.Strings(names)

	for _, name := range names {
		fileNamePrefix := name

		financialClaimsInformationDocument := financialClaimsInformationDocumentMap[name]

		for indexFinancieleZaak, financieleZaak := range financialClaimsInformationDocument.Body.FinancieleZaken {
			if len(financialClaimsInformationDocument.Body.FinancieleZaken) > 1 {
				fileNamePrefix = fmt.Sprintf("%s-%d", name, indexFinancieleZaak+1)
			}

			fileNameSuffix := 1

			if len(financieleZaak.ContactOpties) > 0 {
				filename := fmt.Sprintf("%s-%d-contact.json", fileNamePrefix, fileNameSuffix)

				outputFile, err := os.Create(fmt.Sprintf("dist/demo-json/%s", filename))
				if err != nil {
					return err
				}

				defer outputFile.Close()

				bytes, err := json.Marshal(
					demoSystemModel.CreateContactOpties{
						ContactOpties: financieleZaak.ContactOpties,
						Zaakkenmerk:   financieleZaak.Zaakkenmerk,
					},
				)
				if err != nil {
					return err
				}

				_, err = outputFile.Write(bytes)
				if err != nil {
					return err
				}

				uc.addEventToDemoData(demoData, financialClaimsInformationDocument.Body.AangeleverdDoor, name, filename, model.ContactOpties)

				fileNameSuffix++
			}

			for _, gebeurtenis := range financieleZaak.Gebeurtenissen {
				event := gebeurtenis.(systemModel.Event)

				if slices.Contains(createdGebeurtenisKenmerkSlice, event.GetBaseEvent().GebeurtenisKenmerk) {
					continue
				}

				filename := fmt.Sprintf("%s-%d.json", fileNamePrefix, fileNameSuffix)

				outputFile, err := os.Create(fmt.Sprintf("dist/demo-json/%s", filename))
				if err != nil {
					return err
				}

				defer outputFile.Close()

				bytes, err := json.Marshal(gebeurtenis)
				if err != nil {
					return err
				}

				_, err = outputFile.Write(bytes)
				if err != nil {
					return err
				}

				createdGebeurtenisKenmerkSlice = append(createdGebeurtenisKenmerkSlice, event.GetBaseEvent().GebeurtenisKenmerk)

				uc.addEventToDemoData(demoData, financialClaimsInformationDocument.Body.AangeleverdDoor, name, filename, model.Event)

				fileNameSuffix++
			}

			for _, achterstandenGebeurtenisKenmerk := range financieleZaak.Achterstanden {
				filename := fmt.Sprintf("%s-%d-achterstand.json", fileNamePrefix, fileNameSuffix)

				outputFile, err := os.Create(fmt.Sprintf("dist/demo-json/%s", filename))
				if err != nil {
					return err
				}

				defer outputFile.Close()

				bytes, err := json.Marshal(
					demoSystemModel.CreateAchterstand{
						Zaakkenmerk:        financieleZaak.Zaakkenmerk,
						GebeurtenisKenmerk: achterstandenGebeurtenisKenmerk,
					},
				)
				if err != nil {
					return err
				}

				_, err = outputFile.Write(bytes)
				if err != nil {
					return err
				}

				uc.addEventToDemoData(demoData, financialClaimsInformationDocument.Body.AangeleverdDoor, name, filename, model.Achterstand)

				fileNameSuffix++
			}
		}
	}

	return nil
}

func (uc *ScenarioUsecase) CreateDemoIndexFile(demoData *[]model.IndexOrganization) error {
	data, err := json.Marshal(demoData)
	if err != nil {
		return fmt.Errorf("failed to marshal demo data: %v", err)
	}

	err = os.WriteFile("dist/demo-json/index.json", data, 0644)
	if err != nil {
		return fmt.Errorf("failed to create index file: %v", err)
	}
	fmt.Println("Index file created successfully!")
	return nil
}

func (uc *ScenarioUsecase) addEventToDemoData(organizations *[]model.IndexOrganization, oin, scenarioName, filename string, filetype model.Filetype) {
	var organizationIndex int
	var organizationFound bool
	for i, organization := range *organizations {
		if organization.Oin == oin {
			organizationIndex = i
			organizationFound = true
			break
		}
	}

	if !organizationFound {
		newOrganization := model.IndexOrganization{
			Oin:       oin,
			Scenarios: []model.IndexScenario{},
		}
		*organizations = append(*organizations, newOrganization)
		organizationIndex = len(*organizations) - 1
	}

	var scenarioIndex int
	var scenarioFound bool
	for i, scenario := range (*organizations)[organizationIndex].Scenarios {
		if scenario.Name == scenarioName {
			scenarioIndex = i
			scenarioFound = true
			break
		}
	}

	if !scenarioFound {
		newScenario := model.IndexScenario{
			Name:  scenarioName,
			Files: []model.IndexFile{},
		}
		(*organizations)[organizationIndex].Scenarios = append((*organizations)[organizationIndex].Scenarios, newScenario)
		scenarioIndex = len((*organizations)[organizationIndex].Scenarios) - 1
	}

	(*organizations)[organizationIndex].Scenarios[scenarioIndex].Files = append(
		(*organizations)[organizationIndex].Scenarios[scenarioIndex].Files,
		model.IndexFile{Filename: filename, Type: filetype},
	)
}
