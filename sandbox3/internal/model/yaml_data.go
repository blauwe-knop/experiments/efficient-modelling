// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

import (
	"encoding/json"

	sanbox2Model "gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox/sandbox2/pkg/model"
	systemModel "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"
)

type YamlData struct {
	Scenarios      []Scenario          `json:"scenarios"`
	Gebeurtenissen []systemModel.Event `json:"gebeurtenissen"`
}

func (yamlData *YamlData) UnmarshalJSON(data []byte) error {
	var objectMap map[string]*json.RawMessage

	err := json.Unmarshal(data, &objectMap)
	if err != nil {
		return err
	}

	err = json.Unmarshal(*objectMap["scenarios"], &yamlData.Scenarios)
	if err != nil {
		return err
	}

	gebeurtenisRawMessageSlice := []json.RawMessage{}

	err = json.Unmarshal(*objectMap["gebeurtenissen"], &gebeurtenisRawMessageSlice)
	if err != nil {
		return err
	}

	for _, gebeurtenisRawMessage := range gebeurtenisRawMessageSlice {
		baseEvent := systemModel.BaseEvent{}
		err = json.Unmarshal(gebeurtenisRawMessage, &baseEvent)
		if err != nil {
			return err
		}

		var event systemModel.Event
		switch baseEvent.GebeurtenisType {
		case "FinancieleVerplichtingOpgelegd":
			var sanbox2ModelFinancieleVerplichtingOpgelegdEvent sanbox2Model.FinancieleVerplichtingOpgelegd
			err = json.Unmarshal(gebeurtenisRawMessage, &sanbox2ModelFinancieleVerplichtingOpgelegdEvent)
			if err != nil {
				return err
			}

			var financieleVerplichtingOpgelegdEvent systemModel.FinancieleVerplichtingOpgelegd
			err = json.Unmarshal(gebeurtenisRawMessage, &financieleVerplichtingOpgelegdEvent)
			event = financieleVerplichtingOpgelegdEvent

		case "FinancieleVerplichtingKwijtgescholden":
			var financieleVerplichtingKwijtgescholden systemModel.FinancieleVerplichtingKwijtgescholden
			err = json.Unmarshal(gebeurtenisRawMessage, &financieleVerplichtingKwijtgescholden)

			event = financieleVerplichtingKwijtgescholden

		case "FinancieleVerplichtingGecorrigeerd":
			var financieleVerplichtingGecorrigeerd systemModel.FinancieleVerplichtingGecorrigeerd
			err = json.Unmarshal(gebeurtenisRawMessage, &financieleVerplichtingGecorrigeerd)

			event = financieleVerplichtingGecorrigeerd

		case "BetalingsverplichtingOpgelegd":
			var betalingsverplichtingOpgelegd systemModel.BetalingsverplichtingOpgelegd
			err = json.Unmarshal(gebeurtenisRawMessage, &betalingsverplichtingOpgelegd)

			event = betalingsverplichtingOpgelegd

		case "BetalingsverplichtingIngetrokken":
			var betalingsverplichtingIngetrokken systemModel.BetalingsverplichtingIngetrokken
			err = json.Unmarshal(gebeurtenisRawMessage, &betalingsverplichtingIngetrokken)

			event = betalingsverplichtingIngetrokken

		case "BetalingVerwerkt":
			var betalingVerwerkt systemModel.BetalingVerwerkt
			err = json.Unmarshal(gebeurtenisRawMessage, &betalingVerwerkt)

			event = betalingVerwerkt

		case "FinancieelRechtVastgesteld":
			var financieelRechtVastgesteld systemModel.FinancieelRechtVastgesteld
			err = json.Unmarshal(gebeurtenisRawMessage, &financieelRechtVastgesteld)

			event = financieelRechtVastgesteld

		case "BedragUitbetaald":
			var bedragUitbetaald systemModel.BedragUitbetaald
			err = json.Unmarshal(gebeurtenisRawMessage, &bedragUitbetaald)

			event = bedragUitbetaald

		case "VerrekeningVerwerkt":
			var verrekeningVerwerkt systemModel.VerrekeningVerwerkt
			err = json.Unmarshal(gebeurtenisRawMessage, &verrekeningVerwerkt)

			event = verrekeningVerwerkt

		case "FinancieleZaakOvergedragen":
			var financieleZaakOvergedragen systemModel.FinancieleZaakOvergedragen
			err = json.Unmarshal(gebeurtenisRawMessage, &financieleZaakOvergedragen)

			event = financieleZaakOvergedragen

		case "FinancieleZaakOvergenomen":
			var financieleZaakOvergenomen systemModel.FinancieleZaakOvergenomen
			err = json.Unmarshal(gebeurtenisRawMessage, &financieleZaakOvergenomen)

			event = financieleZaakOvergenomen

		case "FinancieleZaakOvergedragenAanDeurwaarder":
			var financieleZaakOvergedragenAanDeurwaarder systemModel.FinancieleZaakOvergedragenAanDeurwaarder
			err = json.Unmarshal(gebeurtenisRawMessage, &financieleZaakOvergedragenAanDeurwaarder)

			event = financieleZaakOvergedragenAanDeurwaarder

		}

		if err != nil {
			return err
		}

		yamlData.Gebeurtenissen = append(yamlData.Gebeurtenissen, event)
	}

	return nil
}
