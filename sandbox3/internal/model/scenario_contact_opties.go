package model

import systemModel "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"

type ScenarioContactOptie struct {
	Zaakkenmerk   systemModel.Zaakkenmerk    `json:"zaakkenmerk"`
	ContactOpties []systemModel.ContactOptie `json:"contact_opties"`
}
