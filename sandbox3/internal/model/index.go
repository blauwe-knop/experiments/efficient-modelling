package model

type Filetype string

const (
	ContactOpties Filetype = "CONTACT_OPTIES"
	Event         Filetype = "EVENT"
	Achterstand   Filetype = "ACHTERSTAND"
)

type IndexFile struct {
	Filename string   `json:"filename"`
	Type     Filetype `json:"type"`
}

type IndexScenario struct {
	Name  string      `json:"name"`
	Files []IndexFile `json:"files"`
}

type IndexOrganization struct {
	Oin       string          `json:"oin"`
	Scenarios []IndexScenario `json:"scenarios"`
}
