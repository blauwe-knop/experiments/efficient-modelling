package model

import systemModel "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"

type ScenarioAchterstand struct {
	Zaakkenmerk        systemModel.Zaakkenmerk        `json:"zaakkenmerk"`
	GebeurtenisKenmerk systemModel.GebeurtenisKenmerk `json:"gebeurtenis_kenmerk"`
}
