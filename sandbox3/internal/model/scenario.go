// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

import (
	"time"

	systemModel "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"
)

type Scenario struct {
	Name                    string                                                                       `json:"name"`
	DocumentDatumtijd       time.Time                                                                    `json:"document_datumtijd"`
	AangeleverdDoor         string                                                                       `json:"aangeleverd_door"`
	GebeurtenisKenmerken    []systemModel.GebeurtenisKenmerk                                             `json:"gebeurtenis_kenmerken"`
	ScenarioAchterstanden   []ScenarioAchterstand                                                        `json:"scenario_achterstanden,omitempty"`
	ScenarioContactOpties   []ScenarioContactOptie                                                       `json:"scenario_contact_optie,omitempty"`
	ScenarioBeschikbaarheid map[systemModel.FinancieleVerplichtingType]systemModel.BeschikbaarheidStatus `json:"scenario_beschikbaarheid,omitempty"`
	Order                   int                                                                          `json:"order"`
}
