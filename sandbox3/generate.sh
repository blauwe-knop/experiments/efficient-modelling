cd "$(dirname "$0")"


rm -rf dist
mkdir dist
mkdir dist/puml
mkdir dist/svg
mkdir dist/png
mkdir dist/mock-json
mkdir dist/demo-json

go run cmd/sandbox3/*.go

cd plantuml

docker build -t plantuml .

cd ..
rm -rf build

docker run --rm -i -v ${PWD}:/vorijk plantuml -tsvg -o "/vorijk/build" "/vorijk/dist/puml/*.puml"
docker run --rm -i -v ${PWD}:/vorijk plantuml -o "/vorijk/build" "/vorijk/dist/puml/*.puml"

cp build/*.svg dist/svg
cp build/*.png dist/png

prettier dist --write