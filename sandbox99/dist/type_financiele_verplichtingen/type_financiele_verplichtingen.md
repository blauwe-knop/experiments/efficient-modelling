# Type Financiele Verplichtingen

In het onderstaande tabel is er een overzicht van de type financiele verplichtingen binnen het Vorderingenoverzicht Rijk.

| Type                         | Parameter                  | Description                |
| ---------------------------- | -------------------------- | -------------------------- |
| BD_Inkomensheffing           | Heffing inkomstenbelasting | Heffing inkomstenbelasting |
| CJIB_ADMINISTRATIEKOSTEN     | Administratiekosten        | Administratiekosten        |
| CJIB_KOSTEN_EERSTE_AANMANING | Kosten eerste aanmaning    | Kosten eerste aanmaning    |
| CJIB_WAHV                    | Verkeersboete              | Verkeersboete              |
| DUO_LLSF                     | Studieschuld               | Studieschuld               |
| GEMEENTE_AFVALSTOFFENHEFFING | Gemeentebelastingen        | Afvalstoffenheffing        |
| GEMEENTE_OZB_EIGENAAR        | Gemeentebelastingen        | OZB eigenaar               |
| GEMEENTE_RIOOLHEFFING        | Gemeentebelastingen        | Rioolheffing eigenaar      |
