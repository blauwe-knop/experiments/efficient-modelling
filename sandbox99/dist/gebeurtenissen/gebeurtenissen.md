# Gebeurtenissen

In dit document staan de verschillende gebeurtenissen binnen het Vorderingenoverzicht Rijk beschreven.

## BetalingVerwerkt

| Veld                 | Omschrijving                                                  |
| -------------------- | ------------------------------------------------------------- |
| bedrag               | positief bedrag in euro                                       |
| betalingskenmerk     | het betalingskenmerk dat bij de betaling moet worden gebruikt |
| datumTijdGebeurtenis | de datum en tijd waarop de gebeurtenis is geregistreerd       |
| datumTijdOntvangen   | de datum en tijd waarop de betaling is ontvangen              |
| datumTijdVerwerkt    | de datum en tijd waarop de betaling is verwerkt               |
| gebeurtenisKenmerk   | een unieke identifier die de gebeurtenis identificeert        |
| ontvangenDoor        | organisatie OIN                                               |
| verwerktDoor         | organisatie OIN                                               |

## BetalingsverplichtingIngetrokken

| Veld                          | Omschrijving                                            |
| ----------------------------- | ------------------------------------------------------- |
| datumTijdGebeurtenis          | de datum en tijd waarop de gebeurtenis is geregistreerd |
| datumtijdIngetrokken          | het kenmerk van de ingetrokken betalingsverplichting    |
| gebeurtenisKenmerk            | een unieke identifier die de gebeurtenis identificeert  |
| ingetrokkenGebeurtenisKenmerk | het kenmerk van de ingetrokken betalingsverplichting    |

## BetalingsverplichtingOpgelegd

| Veld                         | Omschrijving                                                                                |
| ---------------------------- | ------------------------------------------------------------------------------------------- |
| bedrag                       | positief bedrag in euro                                                                     |
| betaalwijze                  | mogelijke betaalwijze(n)                                                                    |
| betalingskenmerk             | het betalingskenmerk dat bij de betaling moet worden gebruikt                               |
| bsn                          | het bsn van de persoon aan wie de verplichting is opgelegd                                  |
| datumTijdGebeurtenis         | de datum en tijd waarop de gebeurtenis is geregistreerd                                     |
| datumTijdOpgelegd            | datum en tijd waarop de gebeurtenis type is opgelegd                                        |
| gebeurtenisKenmerk           | een unieke identifier die de gebeurtenis identificeert                                      |
| rekeningnummer               | het IBAN rekeningnummer waaraan betaald moet worden                                         |
| rekeningnummerTenaamstelling | de naam van de rekeninghouder                                                               |
| teBetalenAan                 | De organisatie (OIN) waaraan betaald moet worden                                            |
| vervaldatum                  | De datum waarop de betaling binnen moet zijn bij de ontvangende organisatie                 |
| zaakkenmerk                  | een kenmerk van de groep financiële verplichtingen waaraan deze verplichting gerelateerd is |

## FinancieleVerplichtingOpgelegd

| Veld                            | Omschrijving                                                                                                                       |
| ------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------- |
| bedrag                          | positief bedrag in euro                                                                                                            |
| beschikkingsnummer              | nummer van de beschikking behorend bij de financiële verplichting                                                                  |
| bsn                             | het bsn van de persoon aan wie de verplichting is opgelegd                                                                         |
| categorie                       | een optionele aanduiding van de categorie van de verplichting                                                                      |
| datumTijdGebeurtenis            | de datum en tijd waarop de gebeurtenis is geregistreerd                                                                            |
| datumTijdOpgelegd               | datum en tijd waarop de gebeurtenis type is opgelegd                                                                               |
| gebeurtenisKenmerk              | een unieke identifier die de gebeurtenis identificeert                                                                             |
| juridischeGrondslagBron         | een deeplink naar wetten.overheid.nl of andere wetsbeschrijving                                                                    |
| juridischeGrondslagOmschrijving | Wetsartikel op basis waarvan de verplichting is opgelegd                                                                           |
| omschrijving                    | leesbare versie van de juridische grondslag                                                                                        |
| opgelegdDoor                    | organisatie OIN                                                                                                                    |
| primaireVerplichting            | boolean die aangeeft of deze financiële verplichting de primaire financiële verplichting in een groep financiële verplichtingen is |
| uitgevoerdDoor                  | organisatie OIN                                                                                                                    |
| zaakkenmerk                     | een kenmerk van de groep financiële verplichtingen waaraan deze verplichting gerelateerd is                                        |
