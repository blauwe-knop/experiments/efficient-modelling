
cd "$(dirname "$0")"

cd ..

rm -rf dist
mkdir dist
mkdir dist/type_financiele_verplichtingen
mkdir dist/scenarios
mkdir dist/scenarios/puml
mkdir dist/scenarios/svg
mkdir dist/scenarios/png
mkdir dist/gebeurtenissen

go run cmd/sandbox99/*.go

cd plantuml

docker build -t plantuml .

cd ..
rm -rf build

docker run --rm -i -v ${PWD}:/vorijk plantuml -tsvg -o "/vorijk/build/scenarios" "/vorijk/dist/scenarios/puml/*.puml"
docker run --rm -i -v ${PWD}:/vorijk plantuml -o "/vorijk/build/scenarios" "/vorijk/dist/scenarios/puml/*.puml"

cp build/scenarios/*.svg dist/scenarios/svg
cp build/scenarios/*.png dist/scenarios/png

prettier dist --write