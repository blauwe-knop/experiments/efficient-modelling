// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package usecases

import (
	"fmt"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"text/template"
	"time"

	"github.com/deiu/rdf2go"
	"gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox99/internal/model"
	"gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox99/internal/utils"
	sourceSystemModel "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"
	"go.uber.org/zap"
)

type ScenariosUsecase struct {
	Logger *zap.Logger
}

func NewScenariosUsecase(
	logger *zap.Logger,
) *ScenariosUsecase {
	return &ScenariosUsecase{
		Logger: logger,
	}
}

type ScenarioTemplateData struct {
	Name             string
	Fase             int
	AchterstandSlice []model.Achterstand
	GebeurtenisSlice []interface{}
	//TODO FinancialClaimsInformationDocument
}

func (uc *ScenariosUsecase) GenerateScenarioTemplateDatas() (*[]ScenarioTemplateData, error) {
	scenarioTemplateDatas := []ScenarioTemplateData{}

	dirEntries, err := os.ReadDir("ttl/scenario")
	if err != nil {
		return nil, err
	}

	for _, dirEntry := range dirEntries {
		ttlFileName := dirEntry.Name()

		ttlFileNamePeriodIndex := strings.LastIndex(ttlFileName, ".")

		ttlFileNameWithoutExtension := ttlFileName[:ttlFileNamePeriodIndex]

		ttlFile, err := os.Open(fmt.Sprintf("ttl/scenario/%s", ttlFileName))
		if err != nil {
			return nil, err
		}
		defer ttlFile.Close()

		graph := rdf2go.NewGraph(fmt.Sprintf("http://vorijk.nl/scenario/%s#>", ttlFileNameWithoutExtension))

		err = graph.Parse(ttlFile, "text/turtle")
		if err != nil {
			return nil, err
		}

		scenarioTriples := graph.All(nil, nil, rdf2go.NewResource("http://vorijk.nl/ontology#Scenario"))

		for _, scenarioTriple := range scenarioTriples {
			scenarioTemplateData := ScenarioTemplateData{}

			scenarioTemplateData.Name = strings.ToLower(utils.GetTermName(scenarioTriple.Subject))

			fase, err := strconv.Atoi(graph.One(scenarioTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#fase"), nil).Object.RawValue())
			if err != nil {
				return nil, err
			}
			scenarioTemplateData.Fase = fase

			hasGebeurtenisTriples := graph.All(scenarioTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#hasGebeurtenis"), nil)

			sort.Slice(hasGebeurtenisTriples, func(i, j int) bool {
				orderI, _ := getOrder(*graph, hasGebeurtenisTriples[i].Object)

				orderJ, _ := getOrder(*graph, hasGebeurtenisTriples[j].Object)

				return orderI < orderJ
			})

			for _, hasGebeurtenisTriple := range hasGebeurtenisTriples {
				gebeurtenisTriple := graph.One(hasGebeurtenisTriple.Object, rdf2go.NewResource("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), nil)

				gebeurtenisKenmerk := graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#gebeurtenisKenmerk"), nil).Object.RawValue()

				datumTijdGebeurtenis, err := time.Parse(time.RFC3339, graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#datumTijdGebeurtenis"), nil).Object.RawValue())
				if err != nil {
					return nil, err
				}

				switch utils.GetTermName(gebeurtenisTriple.Object) {
				case "BetalingVerwerkt":
					datumTijdVerwerkt, err := time.Parse(time.RFC3339, graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#datumTijdVerwerkt"), nil).Object.RawValue())
					if err != nil {
						return nil, err
					}

					betalingskenmerk := graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#betalingskenmerk"), nil).Object.RawValue()

					datumTijdOntvangen, err := time.Parse(time.RFC3339, graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#datumTijdOntvangen"), nil).Object.RawValue())
					if err != nil {
						return nil, err
					}

					ontvangenDoor := graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#ontvangenDoor"), nil).Object.RawValue()

					verwerktDoor := graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#verwerktDoor"), nil).Object.RawValue()

					bedrag, err := strconv.Atoi(graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#bedrag"), nil).Object.RawValue())
					if err != nil {
						return nil, err
					}

					order, err := getOrder(*graph, gebeurtenisTriple.Subject)
					if err != nil {
						return nil, err
					}

					scenarioTemplateData.GebeurtenisSlice = append(scenarioTemplateData.GebeurtenisSlice, model.BetalingVerwerkt{
						BaseTemplateData: model.BaseTemplateData{
							Name:  utils.GetTermName(gebeurtenisTriple.Subject),
							Order: order,
						},
						BetalingVerwerkt: sourceSystemModel.BetalingVerwerkt{
							DatumtijdVerwerkt:  datumTijdVerwerkt,
							Betalingskenmerk:   betalingskenmerk,
							DatumtijdOntvangen: datumTijdOntvangen,
							OntvangenDoor:      ontvangenDoor,
							VerwerktDoor:       verwerktDoor,
							Bedrag:             bedrag,
							BaseEvent: sourceSystemModel.BaseEvent{
								GebeurtenisType:      "BetalingVerwerkt",
								GebeurtenisKenmerk:   sourceSystemModel.GebeurtenisKenmerk(gebeurtenisKenmerk),
								DatumtijdGebeurtenis: datumTijdGebeurtenis,
							},
						},
					})
				case "BetalingsverplichtingIngetrokken":
					betalingsverplichtingIngetrokkenGebeurtenisKenmerkTriple := graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#ingetrokkenGebeurtenisKenmerk"), nil)

					gebeurtenisKenmerkTriples := graph.All(nil, rdf2go.NewResource("http://vorijk.nl/ontology#gebeurtenisKenmerk"), betalingsverplichtingIngetrokkenGebeurtenisKenmerkTriple.Object)

					for _, gebeurtenisKenmerkTriple := range gebeurtenisKenmerkTriples {
						gebeurtenisKenmerkClassTriple := graph.One(gebeurtenisKenmerkTriple.Subject, rdf2go.NewResource("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), rdf2go.NewResource("http://vorijk.nl/ontology#BetalingsverplichtingOpgelegd"))

						if gebeurtenisKenmerkClassTriple != nil {
							gebeurtenisKenmerk := graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#gebeurtenisKenmerk"), nil).Object.RawValue()

							datumTijdGebeurtenis, err := time.Parse(time.RFC3339, graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#datumTijdGebeurtenis"), nil).Object.RawValue())
							if err != nil {
								return nil, err
							}

							ingetrokkenGebeurtenisKenmerk := graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#ingetrokkenGebeurtenisKenmerk"), nil).Object.RawValue()

							datumTijdIngetrokken, err := time.Parse(time.RFC3339, graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#datumTijdIngetrokken"), nil).Object.RawValue())
							if err != nil {
								return nil, err
							}

							order, err := getOrder(*graph, gebeurtenisTriple.Subject)
							if err != nil {
								return nil, err
							}

							scenarioTemplateData.GebeurtenisSlice = append(
								scenarioTemplateData.GebeurtenisSlice,
								model.BetalingsverplichtingIngetrokken{
									BaseTemplateData: model.BaseTemplateData{
										Name:  utils.GetTermName(gebeurtenisTriple.Subject),
										Order: order,
									},
									BetalingsverplichtingOpgelegdName: utils.GetTermName(gebeurtenisKenmerkClassTriple.Subject),
									BetalingsverplichtingIngetrokken: sourceSystemModel.BetalingsverplichtingIngetrokken{
										IngetrokkenGebeurtenisKenmerk: sourceSystemModel.GebeurtenisKenmerk(ingetrokkenGebeurtenisKenmerk),
										DatumtijdIngetrokken:          datumTijdIngetrokken,
										BaseEvent: sourceSystemModel.BaseEvent{
											GebeurtenisType:      "BetalingsverplichtingIngetrokken",
											GebeurtenisKenmerk:   sourceSystemModel.GebeurtenisKenmerk(gebeurtenisKenmerk),
											DatumtijdGebeurtenis: datumTijdGebeurtenis,
										},
									},
								},
							)
						}
					}
				case "BetalingsverplichtingOpgelegd":
					gebeurtenisKenmerk := graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#gebeurtenisKenmerk"), nil).Object.RawValue()

					datumTijdGebeurtenis, err := time.Parse(time.RFC3339, graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#datumTijdGebeurtenis"), nil).Object.RawValue())
					if err != nil {
						return nil, err
					}

					datumTijdOpgelegd, err := time.Parse(time.RFC3339, graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#datumTijdOpgelegd"), nil).Object.RawValue())
					if err != nil {
						return nil, err
					}

					zaakkenmerk := graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#zaakkenmerk"), nil).Object.RawValue()

					bsn := graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#bsn"), nil).Object.RawValue()

					bedrag, err := strconv.Atoi(graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#bedrag"), nil).Object.RawValue())
					if err != nil {
						return nil, err
					}

					betaalwijze := graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#betaalwijze"), nil).Object.RawValue()

					teBetalenAan := graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#teBetalenAan"), nil).Object.RawValue()

					rekeningnummer := graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#rekeningnummer"), nil).Object.RawValue()

					rekeningnummerTenaamstelling := graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#rekeningnummerTenaamstelling"), nil).Object.RawValue()

					betalingskenmerk := graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#betalingskenmerk"), nil).Object.RawValue()

					vervaldatum, err := time.Parse(time.RFC3339, graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#vervaldatum"), nil).Object.RawValue())
					if err != nil {
						return nil, err
					}

					order, err := getOrder(*graph, gebeurtenisTriple.Subject)
					if err != nil {
						return nil, err
					}

					scenarioTemplateData.GebeurtenisSlice = append(scenarioTemplateData.GebeurtenisSlice, model.BetalingsverplichtingOpgelegd{
						BaseTemplateData: model.BaseTemplateData{
							Name:  utils.GetTermName(gebeurtenisTriple.Subject),
							Order: order,
						},
						BetalingsverplichtingOpgelegd: sourceSystemModel.BetalingsverplichtingOpgelegd{
							DatumtijdOpgelegd:            datumTijdOpgelegd,
							Zaakkenmerk:                  sourceSystemModel.Zaakkenmerk(zaakkenmerk),
							Bsn:                          sourceSystemModel.Bsn(bsn),
							Bedrag:                       bedrag,
							Betaalwijze:                  betaalwijze,
							TeBetalenAan:                 teBetalenAan,
							Rekeningnummer:               rekeningnummer,
							RekeningnummerTenaamstelling: rekeningnummerTenaamstelling,
							Betalingskenmerk:             betalingskenmerk,
							Vervaldatum:                  vervaldatum,
							BaseEvent: sourceSystemModel.BaseEvent{
								GebeurtenisType:      "BetalingsverplichtingOpgelegd",
								GebeurtenisKenmerk:   sourceSystemModel.GebeurtenisKenmerk(gebeurtenisKenmerk),
								DatumtijdGebeurtenis: datumTijdGebeurtenis,
							},
						},
					})

				case "FinancieleVerplichtingOpgelegd":
					gebeurtenisKenmerk := graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#gebeurtenisKenmerk"), nil).Object.RawValue()

					datumTijdGebeurtenis, err := time.Parse(time.RFC3339, graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#datumTijdGebeurtenis"), nil).Object.RawValue())
					if err != nil {
						return nil, err
					}

					datumTijdOpgelegd, err := time.Parse(time.RFC3339, graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#datumTijdOpgelegd"), nil).Object.RawValue())
					if err != nil {
						return nil, err
					}

					zaakkenmerk := graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#zaakkenmerk"), nil).Object.RawValue()

					beschikkingsnummer := graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#beschikkingsnummer"), nil).Object.RawValue()

					bsn := graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#bsn"), nil).Object.RawValue()

					primaireVerplichting, err := strconv.ParseBool(graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#primaireVerplichting"), nil).Object.RawValue())
					if err != nil {
						return nil, err
					}

					typeFinancieleVerplichting := utils.GetTermName(graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#type"), nil).Object)

					categorie := graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#categorie"), nil).Object.RawValue()

					omschrijving := graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#omschrijving"), nil).Object.RawValue()

					juridischeGrondslagOmschrijving := graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#juridischeGrondslagOmschrijving"), nil).Object.RawValue()

					juridischeGrondslagBron := graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#juridischeGrondslagBron"), nil).Object.RawValue()

					opgelegdDoor := graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#opgelegdDoor"), nil).Object.RawValue()

					uitgevoerdDoor := graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#uitgevoerdDoor"), nil).Object.RawValue()

					bedrag, err := strconv.Atoi(graph.One(gebeurtenisTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#bedrag"), nil).Object.RawValue())
					if err != nil {
						return nil, err
					}

					order, err := getOrder(*graph, gebeurtenisTriple.Subject)
					if err != nil {
						return nil, err
					}

					scenarioTemplateData.GebeurtenisSlice = append(scenarioTemplateData.GebeurtenisSlice, model.FinancieleVerplichtingOpgelegd{
						BaseTemplateData: model.BaseTemplateData{
							Name:  utils.GetTermName(gebeurtenisTriple.Subject),
							Order: order,
						},
						FinancieleVerplichtingOpgelegd: sourceSystemModel.FinancieleVerplichtingOpgelegd{
							DatumtijdOpgelegd:               datumTijdOpgelegd,
							Zaakkenmerk:                     sourceSystemModel.Zaakkenmerk(zaakkenmerk),
							Beschikkingsnummer:              beschikkingsnummer,
							Bsn:                             sourceSystemModel.Bsn(bsn),
							PrimaireVerplichting:            primaireVerplichting,
							Type:                            typeFinancieleVerplichting,
							Categorie:                       categorie,
							Bedrag:                          bedrag,
							Omschrijving:                    omschrijving,
							JuridischeGrondslagOmschrijving: juridischeGrondslagOmschrijving,
							JuridischeGrondslagBron:         juridischeGrondslagBron,
							OpgelegdDoor:                    opgelegdDoor,
							UitgevoerdDoor:                  uitgevoerdDoor,
							BaseEvent: sourceSystemModel.BaseEvent{
								GebeurtenisType:      "FinancieleVerplichtingOpgelegd",
								GebeurtenisKenmerk:   sourceSystemModel.GebeurtenisKenmerk(gebeurtenisKenmerk),
								DatumtijdGebeurtenis: datumTijdGebeurtenis,
							},
						},
					})
				}
			}

			hasAchterstandTriples := graph.All(scenarioTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#hasAchterstand"), nil)

			sort.Slice(hasAchterstandTriples, func(i, j int) bool {
				orderI, _ := getOrder(*graph, hasAchterstandTriples[i].Object)

				orderJ, _ := getOrder(*graph, hasAchterstandTriples[j].Object)

				return orderI < orderJ
			})

			for _, hasAchterstandTriple := range hasAchterstandTriples {
				achterstandTriple := graph.One(hasAchterstandTriple.Object, rdf2go.NewResource("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), nil)

				switch utils.GetTermName(achterstandTriple.Object) {
				case "Achterstand":
					achterstandGebeurtenisKenmerkTriple := graph.One(achterstandTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#gebeurtenisKenmerk"), nil)

					zaakkenmerk := graph.One(achterstandTriple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#zaakkenmerk"), nil).Object.RawValue()

					gebeurtenisKenmerkTriples := graph.All(nil, rdf2go.NewResource("http://vorijk.nl/ontology#gebeurtenisKenmerk"), achterstandGebeurtenisKenmerkTriple.Object)

					order, err := getOrder(*graph, achterstandTriple.Subject)
					if err != nil {
						return nil, err
					}

					betalingsverplichtingOpgelegdFound := false
					for _, gebeurtenisKenmerkTriple := range gebeurtenisKenmerkTriples {
						betalingsverplichtingOpgelegdClassTriple := graph.One(gebeurtenisKenmerkTriple.Subject, rdf2go.NewResource("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), rdf2go.NewResource("http://vorijk.nl/ontology#BetalingsverplichtingOpgelegd"))

						if betalingsverplichtingOpgelegdClassTriple != nil {
							betalingsverplichtingOpgelegdFound = true

							scenarioTemplateData.AchterstandSlice = append(
								scenarioTemplateData.AchterstandSlice,
								model.Achterstand{
									BaseTemplateData: model.BaseTemplateData{
										Name:  utils.GetTermName(achterstandTriple.Subject),
										Order: order,
									},
									Zaakkenmerk:                       sourceSystemModel.Zaakkenmerk(zaakkenmerk),
									GebeurtenisKenmerk:                sourceSystemModel.GebeurtenisKenmerk(achterstandGebeurtenisKenmerkTriple.Object.RawValue()),
									BetalingsverplichtingOpgelegdName: utils.GetTermName(betalingsverplichtingOpgelegdClassTriple.Subject),
								},
							)
						}
					}

					if !betalingsverplichtingOpgelegdFound {
						uc.Logger.Error("BetalingsverplichtingOpgelegd not found for achterstand", zap.String("Achterstand.Name", utils.GetTermName(achterstandTriple.Subject)))
						return nil, fmt.Errorf("BetalingsverplichtingOpgelegd not found for achterstand with name: %s", utils.GetTermName(achterstandTriple.Subject))
					}
				}
			}

			scenarioTemplateDatas = append(scenarioTemplateDatas, scenarioTemplateData)
		}
	}
	return &scenarioTemplateDatas, nil
}

func (uc *ScenariosUsecase) GenerateMarkdown(scenarioTemplateDatas []ScenarioTemplateData) error {
	templatePath := filepath.Join("template/plantuml", "scenario.template.puml")
	fileTemplatePath := path.Join(templatePath)

	templateFile, err := template.ParseFiles(fileTemplatePath)
	if err != nil {
		return err
	}

	for _, scenariosTemplateData := range scenarioTemplateDatas {
		outputFile, err := os.Create(fmt.Sprintf("dist/scenarios/puml/%s.puml", scenariosTemplateData.Name))
		if err != nil {
			return err
		}

		defer outputFile.Close()
		err = templateFile.Execute(
			outputFile,
			scenariosTemplateData,
		)

		if err != nil {
			return err
		}
	}

	return nil
}

func getOrder(graph rdf2go.Graph, subject rdf2go.Term) (int, error) {
	orderTriple := graph.One(subject, rdf2go.NewResource("http://vorijk.nl/ontology#order"), nil)

	order, err := strconv.Atoi(orderTriple.Object.RawValue())
	return order, err
}
