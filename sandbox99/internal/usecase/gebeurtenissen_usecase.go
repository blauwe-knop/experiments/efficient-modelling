// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package usecases

import (
	"os"
	"path"
	"path/filepath"
	"sort"
	"text/template"

	"slices"

	"github.com/deiu/rdf2go"
	"gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox99/internal/model"
	"gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox99/internal/utils"
	"go.uber.org/zap"
)

type GebeurtenissenUsecase struct {
	Logger *zap.Logger
}

func NewGebeurtenissenUsecase(
	logger *zap.Logger,
) *GebeurtenissenUsecase {
	return &GebeurtenissenUsecase{
		Logger: logger,
	}
}

type GebeurtenissenTemplateData struct {
	Gebeurtenissen []model.Gebeurtenis
}

func (uc *GebeurtenissenUsecase) GenerateMarkdown() error {
	templatePath := filepath.Join("template/markdown", "gebeurtenissen.template.md")
	fileTemplatePath := path.Join(templatePath)

	templateFile, err := template.ParseFiles(fileTemplatePath)
	if err != nil {
		return err
	}

	outputFile, err := os.Create("dist/gebeurtenissen/gebeurtenissen.md")
	if err != nil {
		return err
	}
	defer outputFile.Close()

	ttlFile, err := os.Open("ttl/ontology/vorijk_ontology.ttl")
	if err != nil {
		return err
	}
	defer ttlFile.Close()

	graph := rdf2go.NewGraph("http://vorijk.nl/ontology#")

	err = graph.Parse(ttlFile, "text/turtle")
	if err != nil {
		return err
	}

	domainGebeurtenisTriples := graph.All(nil, rdf2go.NewResource("http://www.w3.org/2000/01/rdf-schema#domain"), rdf2go.NewResource("http://vorijk.nl/ontology#Gebeurtenis"))

	gebeurtenisFields := []model.GebeurtenisField{}

	ignoreResources := []string{
		"http://vorijk.nl/ontology#order",
		"http://vorijk.nl/ontology#fase",
		"http://vorijk.nl/ontology#type",
	}

	for _, triple := range domainGebeurtenisTriples {
		if slices.Contains(ignoreResources, triple.Subject.RawValue()) {
			continue
		}

		commentTripple := graph.One(triple.Subject, rdf2go.NewResource("http://www.w3.org/2000/01/rdf-schema#comment"), nil)

		gebeurtenisFields = append(gebeurtenisFields, model.GebeurtenisField{
			Name:        utils.GetTermName(triple.Subject),
			Description: commentTripple.Object.RawValue(),
		})
	}

	gebeurtenissen := []model.Gebeurtenis{}

	gebeurtenisTypeTriples := graph.All(nil, rdf2go.NewResource("http://www.w3.org/2000/01/rdf-schema#subClassOf"), rdf2go.NewResource("http://vorijk.nl/ontology#Gebeurtenis"))

	for _, triple := range gebeurtenisTypeTriples {
		gebeurtenisTypeFields := make([]model.GebeurtenisField, len(gebeurtenisFields))
		copy(gebeurtenisTypeFields, gebeurtenisFields)

		domainGebeurtenisTypeTriples := graph.All(nil, rdf2go.NewResource("http://www.w3.org/2000/01/rdf-schema#domain"), triple.Subject)

		for _, secondaryTriple := range domainGebeurtenisTypeTriples {
			if slices.Contains(ignoreResources, secondaryTriple.Subject.RawValue()) {
				continue
			}

			commentTripple := graph.One(secondaryTriple.Subject, rdf2go.NewResource("http://www.w3.org/2000/01/rdf-schema#comment"), nil)

			gebeurtenisTypeFields = append(
				gebeurtenisTypeFields,
				model.GebeurtenisField{
					Name:        utils.GetTermName(secondaryTriple.Subject),
					Description: commentTripple.Object.RawValue(),
				},
			)
		}

		gebeurtenissen = append(
			gebeurtenissen,
			model.Gebeurtenis{
				Type:   utils.GetTermName(triple.Subject),
				Fields: gebeurtenisTypeFields,
			},
		)
	}

	sort.Slice(gebeurtenissen, func(i, j int) bool {
		return gebeurtenissen[i].Type < gebeurtenissen[j].Type
	})

	for _, gebeurtenis := range gebeurtenissen {
		sort.Slice(gebeurtenis.Fields, func(i, j int) bool {
			return gebeurtenis.Fields[i].Name < gebeurtenis.Fields[j].Name
		})
	}

	err = templateFile.Execute(
		outputFile,
		&GebeurtenissenTemplateData{
			gebeurtenissen,
		},
	)

	if err != nil {
		return err
	}

	return nil
}
