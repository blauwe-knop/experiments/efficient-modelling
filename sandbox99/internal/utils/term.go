package utils

import (
	"strings"

	"github.com/deiu/rdf2go"
)

func GetTermName(term rdf2go.Term) string {
	uri := term.RawValue()

	index := strings.LastIndex(uri, "#") + 1

	if index > 0 {
		return uri[index:]
	}

	index = strings.LastIndex(uri, "/") + 1

	if index > 0 {
		return uri[index:]
	}

	return uri
}
