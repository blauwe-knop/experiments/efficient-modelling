package model

import sourceSystemModel "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"

type BaseTemplateData struct {
	Name  string
	Order int
}

type FinancieleVerplichtingOpgelegd struct {
	BaseTemplateData
	sourceSystemModel.FinancieleVerplichtingOpgelegd
}

type BetalingsverplichtingOpgelegd struct {
	BaseTemplateData
	sourceSystemModel.BetalingsverplichtingOpgelegd
}

type BetalingsverplichtingIngetrokken struct {
	BetalingsverplichtingOpgelegdName string
	BaseTemplateData
	sourceSystemModel.BetalingsverplichtingIngetrokken
}

type BetalingVerwerkt struct {
	BaseTemplateData
	sourceSystemModel.BetalingVerwerkt
}

type Achterstand struct {
	BetalingsverplichtingOpgelegdName string
	BaseTemplateData
	Zaakkenmerk        sourceSystemModel.Zaakkenmerk
	GebeurtenisKenmerk sourceSystemModel.GebeurtenisKenmerk
}
