// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

import (
	"time"

	sourceSystemModel "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"
)

type FinancialClaimsInformationDocumentFinancieleZaak struct {
	sourceSystemModel.FinancialClaimsInformationDocumentFinancieleZaak
}

type FinancialClaimsInformationDocumentBody struct {
	AangeleverdDoor   string                                             `json:"aangeleverd_door"`
	DocumentDatumtijd time.Time                                          `json:"document_datumtijd"`
	Bsn               sourceSystemModel.Bsn                              `json:"bsn"`
	FinancieleZaken   []FinancialClaimsInformationDocumentFinancieleZaak `json:"financiele_zaken"`
}

type FinancialClaimsInformationDocument struct {
	Type    string                                 `json:"type" default:"FINANCIAL_CLAIMS_INFORMATION_DOCUMENT_V3"`
	Version string                                 `json:"version" default:"3"`
	Body    FinancialClaimsInformationDocumentBody `json:"body"`
}
