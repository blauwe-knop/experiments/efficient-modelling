// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

type TypeFinancieleVerplichting struct {
	Type               string
	FinancieleZaaknaam string
	GebeurtenisNaam    string
}
