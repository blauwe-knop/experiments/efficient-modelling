# Type Financiele Verplichtingen

In het onderstaande tabel is er een overzicht van de type financiele verplichtingen binnen het Vorderingenoverzicht Rijk.

| Type                                    | Parameter | Description             |
| --------------------------------------- | --------- | ----------------------- | -------------------- |
{{range .TypeFinancieleVerplichtingen}} | {{.Type}} | {{.FinancieleZaaknaam}} | {{.GebeurtenisNaam}} |
{{end}}