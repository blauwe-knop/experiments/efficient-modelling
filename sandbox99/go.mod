module gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox99

go 1.21.4

require (
	github.com/deiu/rdf2go v0.0.0-20230522072227-c39eb296d51c
	github.com/jessevdk/go-flags v1.5.0
	github.com/thessem/zap-prettyconsole v0.3.0
	gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system v0.16.0
	go.uber.org/zap v1.26.0
)

require (
	github.com/Code-Hex/dd v1.1.0 // indirect
	github.com/deiu/gon3 v0.0.0-20230411081920-f0f8f879f597 // indirect
	github.com/linkeddata/gojsonld v0.0.0-20170418210642-4f5db6791326 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/rychipman/easylex v0.0.0-20160129204217-49ee7767142f // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/sys v0.14.0 // indirect
)
