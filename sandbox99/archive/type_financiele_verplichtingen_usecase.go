// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package usecases

import (
	"os"
	"path"
	"path/filepath"
	"sort"
	"text/template"

	"github.com/deiu/rdf2go"
	"gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox99/internal/model"
	"gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox99/internal/utils"
	"go.uber.org/zap"
)

type TypeFinancieleVerplichtingenUsecase struct {
	Logger *zap.Logger
}

func NewTypeFinancieleVerplichtingenUsecase(
	logger *zap.Logger,
) *TypeFinancieleVerplichtingenUsecase {
	return &TypeFinancieleVerplichtingenUsecase{
		Logger: logger,
	}
}

type TypeFinancieleVerplichtingenTemplateData struct {
	TypeFinancieleVerplichtingen []model.TypeFinancieleVerplichting
}

func (uc *TypeFinancieleVerplichtingenUsecase) GenerateTypeFinancieleVerplichtingen() (*[]model.TypeFinancieleVerplichting, error) {

	ttlFile, err := os.Open("ttl/type_financiele_verplichtingen.ttl")
	if err != nil {
		return nil, err
	}
	defer ttlFile.Close()

	graph := rdf2go.NewGraph("http://vorijk.nl/type_financiele_verplichtingen#")

	err = graph.Parse(ttlFile, "text/turtle")
	if err != nil {
		return nil, err
	}

	typeFinancieleVerplichtingen := []model.TypeFinancieleVerplichting{}

	typeFinancieleVerplichtingSubjects := graph.All(nil, nil, rdf2go.NewResource("http://vorijk.nl/ontology#TypeFinancieleVerplichting"))

	for _, triple := range typeFinancieleVerplichtingSubjects {
		financieleZaakNaamTriple := graph.One(triple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#financieleZaakNaam"), nil)

		gebeurtenisNaamTriple := graph.One(triple.Subject, rdf2go.NewResource("http://vorijk.nl/ontology#gebeurtenisNaam"), nil)

		typeFinancieleVerplichtingen = append(
			typeFinancieleVerplichtingen,
			model.TypeFinancieleVerplichting{
				Type:               utils.GetTermName(triple.Subject),
				FinancieleZaaknaam: financieleZaakNaamTriple.Object.RawValue(),
				GebeurtenisNaam:    gebeurtenisNaamTriple.Object.RawValue(),
			},
		)
	}

	sort.Slice(typeFinancieleVerplichtingen, func(i, j int) bool {
		return typeFinancieleVerplichtingen[i].Type < typeFinancieleVerplichtingen[j].Type
	})

	return &typeFinancieleVerplichtingen, nil
}

func (uc *TypeFinancieleVerplichtingenUsecase) GenerateMarkdown(typeFinancieleVerplichtingen []model.TypeFinancieleVerplichting) error {
	templatePath := filepath.Join("template/markdown", "type_financiele_verplichtingen.template.md")

	fileTemplatePath := path.Join(templatePath)

	templateFile, err := template.ParseFiles(fileTemplatePath)
	if err != nil {
		return err
	}

	outputFile, err := os.Create("dist/type_financiele_verplichtingen/type_financiele_verplichtingen.md")
	if err != nil {
		return err
	}
	defer outputFile.Close()

	err = templateFile.Execute(
		outputFile,
		&TypeFinancieleVerplichtingenTemplateData{
			typeFinancieleVerplichtingen,
		},
	)

	if err != nil {
		return err
	}

	return nil
}
