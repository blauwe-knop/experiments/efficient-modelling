// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package main

import (
	"fmt"
	"log"

	usecases "gitlab.com/blauwe-knop/experiments/efficient-modelling/sandbox99/internal/usecase"
	"go.uber.org/zap"
)

type options struct {
	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	logger, err := newZapLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	typeFinancieleVerplichtingenUsecase := usecases.NewTypeFinancieleVerplichtingenUsecase(
		logger,
	)

	typeFinancieleVerplichtingen, err := typeFinancieleVerplichtingenUsecase.GenerateTypeFinancieleVerplichtingen()
	if err != nil {
		logger.Fatal("--- TypeFinancieleVerplichtingen GenerateTypeFinancieleVerplichtingen: FAILED ---", zap.Error(err))
	} else {
		logger.Info("--- TypeFinancieleVerplichtingen GenerateTypeFinancieleVerplichtingen: SUCCEEDED ---")
	}

	err = typeFinancieleVerplichtingenUsecase.GenerateMarkdown(*typeFinancieleVerplichtingen)
	if err != nil {
		logger.Fatal("--- TypeFinancieleVerplichtingen GenerateMarkdown: FAILED ---", zap.Error(err))
	} else {
		logger.Info("--- TypeFinancieleVerplichtingen GenerateMarkdown: SUCCEEDED ---")
	}

	gebeurtenissenUsecase := usecases.NewGebeurtenissenUsecase(
		logger,
	)

	err = gebeurtenissenUsecase.GenerateMarkdown()
	if err != nil {
		logger.Fatal("--- Gebeurtenissen GenerateMarkdown: FAILED ---", zap.Error(err))
	} else {
		logger.Info("--- Gebeurtenissen GenerateMarkdown: SUCCEEDED ---")
	}

	scenariosUsecase := usecases.NewScenariosUsecase(
		logger,
	)

	scenarioTemplateDatas, err := scenariosUsecase.GenerateScenarioTemplateDatas()
	if err != nil {
		logger.Fatal("--- Scenarios GenerateScenarioTemplateDatas: FAILED ---", zap.Error(err))
	} else {
		logger.Info("--- Scenarios GenerateScenarioTemplateDatas: SUCCEEDED ---")
	}

	err = scenariosUsecase.GenerateMarkdown(*scenarioTemplateDatas)
	if err != nil {
		logger.Fatal("--- Scenarios GenerateMarkdown: FAILED ---", zap.Error(err))
	} else {
		logger.Info("--- Scenarios GenerateMarkdown: SUCCEEDED ---")
	}

}

func newZapLogger(logOptions LogOptions) (*zap.Logger, error) {
	config := logOptions.ZapConfig()
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return logger, nil
}
